@extends('dashboard.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(isset($contactRequest))
        <div class="card">
          <div class="card-header">
            <h5 class="title">View Contact Request</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-4">
                <label>Name:</label>
                @if(!empty($contactRequest->name))
                <span>{{$contactRequest->name}}</span>
                @endif
              </div>
              <div class="col-md-4">
                <label>Phone:</label>
                @if(!empty($contactRequest->phone))
                <span>{{$contactRequest->phone}}</span>
                @endif
              </div>
              <div class="col-md-4">
                <label>Email:</label>
                @if(!empty($contactRequest->email))
                <span>{{$contactRequest->email}}</span>
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <label>Subject:</label>
                @if(!empty($contactRequest->subject))
                <span>{{$contactRequest->subject}}</span>
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <label>Message:</label>
                @if(!empty($contactRequest->message))
                <span>{{$contactRequest->message}}</span>
                @endif
              </div>
            </div>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Contact Requests</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $requests = \App\ContactRequest::all();
                  ?>
                  @foreach($requests as $request)
                  <tr>
                    <td>{{$request->id}}</td>
                    <td>{{$request->name}}</td>
                    <td>{{$request->email}}</td>
                    <td>{{$request->phone}}</td>
                    <td>{{$request->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-primary btn-sm" href="/contactrequests/<?php echo $request->id; ?>">View</a>
                      <a type="link" class="btn btn-default btn-sm" href="/contactrequests_changestatus/<?php echo $request->id;?>">Change Status</a>
                      <a type="link" class="btn btn-danger btn-sm" href="/contactrequests_delete/<?php echo $request->id;?>">Delete</a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
