<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('tours', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('title');
          $table->string('place');
          $table->enum('type', ['Umrah', 'Custom']);
          $table->integer('price')->nullable();
          $table->longtext('details');
          $table->enum('status', ['Active', 'Deactive'])->default('Active');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
}
