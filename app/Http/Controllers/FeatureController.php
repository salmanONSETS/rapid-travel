<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;

class FeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.features');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $feature = new \App\Feature;
        $feature->title = $request->get('title');
        $feature->description = $request->get('description');
        if($request->hasFile('image')){
          $destinationImages='feature/images';
          $allowedImagefileExtension=['jpg','png','jpeg'];
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $feature->picturePath = $filename;
          }
          else{
            $message = 'Only .png, .jpg, .jpeg accepted';
            return redirect('features')->with('message', $message);
          }
        }
        $feature->status = 'Active';
        $feature->save();
        return redirect('features');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feature = \App\Feature::find($id);
        return view('dashboard.features')->with('feature', $feature);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $feature = \App\Feature::find($id);
      $feature->title = $request->get('title');
      $feature->description = $request->get('description');
      if($request->hasFile('image')){
        $destinationImages='feature/images';
        $allowedImagefileExtension=['jpg','png','jpeg'];
        $file=Input::file("image");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          if(!empty($feature->picturePath)){
            $path = 'feature/images/'.$feature->picturePath;
            File::delete($path);
          }
          $file->move($destinationImages,$filename);
          $feature->picturePath = $filename;
        }
        else{
          $message = 'Only .png, .jpg, .jpeg accepted';
          return redirect('features')->with('message', $message);
        }
      }
      $feature->status = 'Active';
      $feature->save();
      return redirect('features');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $feature = \App\Feature::find($id);
      if(!empty($feature->picturePath)){
        $path = 'feature/images/'.$feature->picturePath;
        File::delete($path);
      }
      $feature->delete();
      return redirect('features');
    }

    public function changestatus($id){
      $feature = \App\Feature::find($id);
      if($feature->status == 'Active'){
        $feature->status = 'Deactive';
      }
      else{
        $feature->status = 'Active';
      }
      $feature->save();
      return redirect('features');
    }
}
