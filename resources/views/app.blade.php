<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta charset="UTF-8">
      <meta name="description" content="Rapid Travel - Tour and Travel Agency Website">
      <meta name="keywords" content="Rapid Travel, Travel, Tour, Hotels, Cars, Flights">
      <meta name="author" content="ONSETS">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Rapid Travel</title>
      <link rel="icon" href="{{ asset('./img/logo.png') }}" type="image" sizes="16x16">
      <!--bootstrap.min.css -->
      <link href="{{ asset('./css/bootstrap.min.css') }}" rel="stylesheet">
      <!--font-awesome.min.css -->
      <link href="{{ asset('./css/font-awesome.min.css') }}" rel="stylesheet">
      <!--material-design-iconic-font.min.css -->
      <link href="{{ asset('./css/material-design-iconic-font.min.css') }}" rel="stylesheet">
      <!--owl.carousel.min.css -->
      <link href="{{ asset('./css/owl.carousel.min.css') }}" rel="stylesheet">
      <!--magnific-popup.css -->
      <link href="{{ asset('./css/magnific-popup.css') }}" rel="stylesheet">
      <!--nice-select.css -->
      <link href="{{ asset('./css/nice-select.css') }}" rel="stylesheet">
      <!--slicknav.min.css -->
      <link href="{{ asset('./css/slicknav.min.css') }}" rel="stylesheet">
      <!--style.css -->
      <link href="{{ asset('./css/style.min.css') }}" rel="stylesheet">
      <!--responsive.css -->
      <link href="{{ asset('./css/responsive.min.css') }}" rel="stylesheet">

      <link href="{{ asset('./css/demo.css') }}" rel="stylesheet">
    </head>
    <body>
      <div class="welcome-area" style="height:auto">
        <div class="header-area header-absolute">
          <div class="container">
            <div class="row">
              <div class="col-md-2 col-sm-4 col-xs-6">
                <div class="logo">
                  <a href="/"><img src="{{ asset('./img/logo.png') }}" alt=""></a>
                </div>
              </div>
              <div class="col-md-8 col-sm-8">
                <div class="mainmenu">
                  <ul id="slicknav-menu-list">
                    <li><a href="/">Home</a></li>
                    <li><a href="/viewTours">Tours</a></li>
                    <li><a href="/viewPackages">Umrah Packages</a></li>
                    <li><a href="/viewTeamMembers">Team</a></li>
                    <li><a href="/about">About</a></li>
                    <li><a href="/contact">Contact Us</a></li>
                  </ul>
                </div>
              </div>
              <div class="col-md-2 col-sm-8 col-xs-6">
                <div class="slicknav-menu-wrap"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      @yield('content')
      <div class="footer-area section-padding">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-sm-6">
              <div class="footer-widget para-widget">
                <img src="{{ asset('./img/footer-logo.png') }}" class="footer-logo" alt="">
                <p>&copy; <script type = "text/javascript">
                   var dt = new Date();
                   document.write(dt.getFullYear() );
                </script>  <a href="http://onsets.co/">ONSETS</a>. All Rights Reserved.<br>
                  Developed by <a href="http://onsets.co/">ONSETS</a>
                </p>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="footer-widget list-widget">
                <h4>Information</h4>
                <ul>
                  <li><a href="/">Home</a></li>
                  <li><a href="/viewTours">Tours</a></li>
                  <li><a href="/viewPackages">Packages</a></li>
                  <li><a href="/viewTeamMembers">Team</a></li>
                  <li><a href="/about">About Us</a></li>
                  <li><a href="/contact">Contact Us</a></li>
                  <li><a href="/login-panel">Login</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <?php
              $contact = \App\Contact::where('type', 'Primary')->first();
              $address = \App\Address::where('type', 'Primary')->first();
              $email = \App\Email::where('type', 'Primary')->first();
              ?>
              <div class="footer-widget pink-icon-widget">
                <h4>Contact Us</h4>
                <ul>
                  @if(!empty($contact))
                  <li>
                    @if(!empty($contact->title))
                    <p hidden><b>{{$contact->title}}</b></p>
                    @endif
                    <a href="tel:{{$contact->contact}}">{{$contact->contact}}</a>
                  </li>
                  @endif
                  @if(!empty($email))
                  <li>
                    @if(!empty($email->title))
                    <p><b>{{$email->title}}</b></p>
                    @endif
                    <a href="mailto:{{$email->email}}">{{$email->email}}</a>
                  </li>
                  @endif
                  @if(!empty($address))
                  <li>
                    @if(!empty($address->title))
                    <p><b>{{$address->title}}</b></p>
                    @endif
                    <a href="javascript:void(0)">{{$address->address}} </a>
                  </li>
                  @endif
                </ul>
                <?php
                $social = \App\Social::where('status', 'Active')->first();
                ?>
                @if($social)
                <div class="social-links-icon">
                  @if($social->facebook)
                  <a href="{{$social->facebook}}"><i class="fa fa-facebook"></i></a>
                  @endif
                  @if($social->twitter)
                  <a href="{{$social->twitter}}"><i class="fa fa-twitter"></i></a>
                  @endif
                  @if($social->instagram)
                  <a href="{{$social->instagram}}"><i class="fa fa-instagram"></i></a>
                  @endif
                  @if($social->google)
                  <a href="{{$social->google}}"><i class="fa fa-google-plus"></i></a>
                  @endif
                </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>

      <!--jquery.min.js-->
      <script src="{{ asset('./js/jquery.min.js') }}"></script>
      <!--bootstrap.min.js-->
      <script src="{{ asset('./js/bootstrap.min.js') }}"></script>
      <!--owl.carousel.min.js-->
      <script src="{{ asset('./js/owl.carousel.min.js') }}"></script>
      <!--magnific-popup-1.1.0.js-->
      <script src="{{ asset('./js/magnific-popup-1.1.0.js') }}"></script>
      <!--jquery.nice-select.min.js-->
      <script src="{{ asset('./js/jquery.nice-select.min.js') }}"></script>
      <!--jquery.waypoints.4.0.0.min.js-->
      <script src="{{ asset('./js/jquery.waypoints.4.0.0.min.js') }}"></script>
      <!--jquery.counterup.min.js-->
      <script src="{{ asset('./js/jquery.counterup.min.js') }}"></script>
      <!--jquery.slicknav.min.js-->
      <script src="{{ asset('./js/jquery.slicknav.min.js') }}"></script>
      <!--main.js-->
      <script src="{{ asset('./js/main.min.js') }}"></script>

      <!-- bootstrap notify -->
      <script src="{{ asset('./js/plugins/bootstrap-notify.min.js') }}" type="text/javascript"></script>
      @if(session('message'))
      <script type="text/javascript">
      $(document).ready(function(){
        $.notify({
          message: "{{session('message')}}"
        },
        {
          type: 'success',
          allow_dismiss: true,
          timer: 4000
        });
      });
      </script>
      @endif
      <script>
      var dateControl = document.querySelectorAll('input[type="date"]');
      var date = new Date();
      let year = date.getFullYear();
      let month = date.getMonth();
      let day = date.getDate();
      month+=1;
      year = year.toString();
      if(month < 10){
        month = '0' + month.toString();
      }
      if(day < 10){
        day = '0' + day.toString();
      }
      let date1 = year + '-' + month + '-' + day;
      var i;
      for (i = 0; i < dateControl.length; i++) {
        dateControl[i].min = date1;
      }
      </script>
    </body>
</html>
