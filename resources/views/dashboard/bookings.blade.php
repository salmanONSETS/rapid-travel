@extends('dashboard.dashboardlayout')
@section('content')
<div class="content">
  <div class="row">
    <div class="col-md-12">
      @if(isset($booking))
      <div class="card">
        <div class="card-header">
          <h5 class="title">View Booking</h5>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-4">
              <label>Name:</label>
              @if(!empty($booking->name))
              <span>{{$booking->name}}</span>
              @endif
            </div>
            <div class="col-md-4">
              <label>Phone:</label>
              @if(!empty($booking->phone))
              <span>{{$booking->phone}}</span>
              @endif
            </div>
            <div class="col-md-4">
              <label>Email:</label>
              @if(!empty($booking->email))
              <span>{{$booking->email}}</span>
              @endif
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label>Message:</label>
              <span>{{$booking->message}}</span>
            </div>
          </div>
        </div>
      </div>
      @endif
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Bookings</h4>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table">
              <thead class=" text-primary">
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Phone</th>
                  <th>Email</th>
                  <th>Booking For</th>
                  <th>Status</th>
                  <th class="text-right">Control Section</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $bookings = \App\Booking::all();
                ?>
                @foreach($bookings as $booking)
                <tr>
                  <td>{{$booking->id}}</td>
                  <td>{{$booking->name}}</td>
                  <td>{{$booking->phone}}</td>
                  <td>{{$booking->email}}</td>
                  <td>
                    <?php
                    $tour = \App\Tour::find($booking->tourId);
                    ?>
                    @if($tour->type === 'Custom')
                    <a href="/tours/{{$tour->id}}">{{$booking->tourId}}</a>
                    @else
                    <a href="/packages/{{$tour->id}}">{{$booking->tourId}}</a>
                    @endif
                  </td>
                  <td>{{$booking->status}}</td>
                  <td class="text-right">
                    <a type="link" class="btn btn-default btn-sm" href="/bookings_changestatus/<?php echo $booking->id;?>">Change Status</a>
                    <a type="link" class="btn btn-warning btn-sm" href="/bookings/<?php echo $booking->id;?>">View</a>
                    <form action="{{ route('bookings.destroy', $booking->id) }}" method="post" style="display:inline">
                      {{ method_field('DELETE') }}
                      {{ csrf_field() }}
                      <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                    </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
