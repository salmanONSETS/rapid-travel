@extends('dashboard.dashboardlayout')
@section('content')
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <?php
      $aboutusAll = \App\Aboutus::all();
      ?>
      @if(!isset($aboutus) && $aboutusAll->isEmpty())
      <div class="card">
        <div class="card-header">
          <h5 class="title">Add About Us</h5>
        </div>
        <div class="card-body">
          <form role="form" action="/aboutus" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Title</label>
                  <input type="text" class="form-control" placeholder="Title" name="title" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Large Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                  <button class="btn btn-primary">
                    Choose Files<input type="file" class="form-control" name="largeImage" accept=".jpg,.jpeg,.png">
                  </button>
                </div>
              </div>
              <div class="col-md-6" hidden>
                <div class="form-group">
                  <label>Small Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                  <button class="btn btn-primary">
                    Choose Files<input type="file" class="form-control" name="smallImage" accept=".jpg,.jpeg,.png">
                  </button>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>Details</label>
                  <textarea rows="10" class="form-control" placeholder="Details" name="detail"></textarea>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
          </form>
        </div>
      </div>
      @else
      <div class="card">
        <?php
        $aboutus = \App\Aboutus::where('status', 'Active')->first();
        ?>
        <div class="card-header">
          <h5 class="title">Edit About us</h5>
        </div>
        <div class="card-body">
          <div class="row">
            @if(!empty($aboutus->largeImage))
            <div class="col-md-6 text-center">
              <img src="{{asset('aboutpage/images/').'/'.$aboutus->largeImage}}" alt="Large Image" />
              <a type="link" class="btn btn-danger btn-sm" href="/deleteLargeImage/<?php echo $aboutus->id; ?>">Delete</a>
            </div>
            @endif
            @if(!empty($aboutus->smallImage))
            <div class="col-md-6 text-center">
              <img src="{{asset('aboutpage/images/').'/'.$aboutus->smallImage}}" alt="Small Image" />
              <a type="link" class="btn btn-danger btn-sm" href="/deleteSmallImage/<?php echo $aboutus->id; ?>">Delete</a>
            </div>
            @endif
          </div>

          <form role="form" action="/aboutus/<?php echo $aboutus->id;?>" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{method_field('PUT')}}
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Title</label>
                  <input type="text" class="form-control" placeholder="Title" name="title" required value="{{$aboutus->title}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Large Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                  <button class="btn btn-primary">
                    Choose Files<input type="file" class="form-control" name="largeImage" accept=".jpg,.jpeg,.png">
                  </button>
                </div>
              </div>
              <div class="col-md-6" hidden>
                <div class="form-group">
                  <label>Small Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                  <button class="btn btn-primary">
                    Choose Files<input type="file" class="form-control" name="smallImage" accept=".jpg,.jpeg,.png">
                  </button>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>Details</label>
                  <textarea rows="10" class="form-control" placeholder="Details" name="detail">{{$aboutus->detail}}</textarea>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
          </form>
        </div>
      </div>
      @endif
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">About Us</h4>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table">
              <thead class=" text-primary">
                <tr>
                  <th>Id</th>
                  <th>Title</th>
                  <th>Large Image</th>
                  <th hidden>Small Image</th>
                  <th>Status</th>
                  <th class="text-right">Control Section</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $aboutus = \App\Aboutus::all();
                ?>
                @foreach($aboutus as $value)
                <tr>
                  <td>{{$value->id}}</td>
                  <td>{{$value->title}}</td>
                  <td>
                    @if(!empty($value->largeImage))
                    <img src="{{asset('aboutpage/images/').'/'.$value->largeImage}}" alt="Large Image" width="100" height="100" />
                    @endif
                  </td>
                  <td hidden>
                    @if(!empty($value->smallImage))
                    <img src="{{asset('aboutpage/images/').'/'.$value->smallImage}}" alt="Small Image" width="100" height="100" />
                    @endif
                  </td>
                  <td>{{$value->status}}</td>
                  <td class="text-right">
                    <a type="link" class="btn btn-default btn-sm" href="/aboutus_changestatus/<?php echo $value->id;?>">Change Status</a>
                    <a type="link" class="btn btn-warning btn-sm" href="/about">View</a>
                    <a type="link" class="btn btn-info btn-sm" href="/aboutus/<?php echo $value->id;?>/edit">Edit</a>
                    <form action="{{ route('aboutus.destroy', $value->id) }}" method="post" style="display:inline">
                      {{ method_field('DELETE') }}
                      {{ csrf_field() }}
                      <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                    </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
