<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.bookings');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $booking = new \App\Booking;
        $booking->name = $request->get('name');
        $booking->phone = $request->get('phone');
        $booking->tourId = $request->get('tourId');
        $booking->email = $request->get('email');
        $booking->status = 'Active';
        if($request->get('message') !== ''){
            $booking->message = $request->get('message');
        }
        $booking->save();
        $tour = \App\Tour::find($booking->tourId);
        if($tour->type === 'Custom'){
            $url = "/tours/".$booking->tourId;
        }
        else{
          $url = "/packages/".$booking->tourId;
        }
        $message = 'Booking Query Sent';
        return redirect($url)->with('message', $message);
        //return redirect($url, compact('message', 'tour'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking = \App\Booking::find($id);
        return view('dashboard.bookings')->with('booking', $booking);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $booking = \App\Booking::find($id);
        $booking->delete();
        return redirect('/bookings');
    }

    public function changestatus($id){
      $booking = \App\Booking::find($id);
      if($booking->status == 'Active'){
        $booking->status = 'Deactive';
      }
      else{
        $booking->status = 'Active';
      }
      $booking->save();
      return redirect('/bookings');
    }
}
