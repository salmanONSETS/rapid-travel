@extends('dashboard.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($teamMember))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Team Member</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/teamMembers" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" placeholder="Name" name="name" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Designation</label>
                    <input type="text" class="form-control" placeholder="Designation" name="designation" required>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png">
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Team Member</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/teamMembers/<?php echo $teamMember->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" placeholder="Name" name="name" required value="{{$teamMember->name}}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Designation</label>
                    <input type="text" class="form-control" placeholder="Designation" name="designation" value="{{$teamMember->designation}}">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label><small> (.png, .jpg, .jpeg accepted)</small><br>
                    <small>If you want to change the old image you can upload the new one it will replace the old one. Otherwise, leave it empty.</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png">
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Team Members</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Designation</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $teamMembers = \App\TeamMember::all();
                  ?>
                  @foreach($teamMembers as $teamMember)
                  <tr>
                    <td>{{$teamMember->id}}</td>
                    <td><img src="{{asset('team/images/').'/'.$teamMember->picturePath}}" alt="Team Member Image" width="100" height="100" /></td>
                    <td>{{$teamMember->name}}</td>
                    <td>{{$teamMember->designation}}</td>
                    <td>{{$teamMember->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/teamMembers_changestatus/<?php echo $teamMember->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/teamMembers/<?php echo $teamMember->id;?>/edit">Edit</a>
                      <form action="{{ route('teamMembers.destroy', $teamMember->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
