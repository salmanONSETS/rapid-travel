@extends('dashboard.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($toast))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Toast</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/toasts" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-6" hidden>
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Useage*</label>
                    <select class="form-control" name="useage" required>
                      <option>Select Useage</option>
                      <option value="ContactReqest">For Contact Requests</option>
                      <option value="Query">For Queries</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Message*</label>
                    <input type="text" class="form-control" placeholder="Message" name="message">
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Toast</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/toasts/<?php echo $toast->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-6" hidden>
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" required value="{{$toast->title}}">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Useage*</label>
                    <select class="form-control" name="useage" required>
                      <option>Select Useage</option>
                      @if($toast->useage == 'ContactReqest')
                      <option value="ContactReqest" selected>For Contact Requests</option>
                      @else
                      <option value="ContactReqest">For Contact Requests</option>
                      @endif
                      @if($toast->useage == 'Query')
                      <option value="Query" selected>For Queries</option>
                      @else
                      <option value="Query">For Queries</option>
                      @endif
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Message*</label>
                    <input type="text" class="form-control" placeholder="Message" name="message" required value="{{$toast->message}}">
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Toasts</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th hidden>Title</th>
                    <th>Message</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $toasts = \App\Toast::all();
                  ?>
                  @foreach($toasts as $value)
                  <tr>
                    <td>{{$value->id}}</td>
                    <td hidden>{{$value->title}}</td>
                    <td>{{$value->message}}</td>
                    <td>{{$value->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/toasts_changestatus/<?php echo $value->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/toasts/<?php echo $value->id;?>/edit">Edit</a>
                      <form action="{{ route('toasts.destroy', $value->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
