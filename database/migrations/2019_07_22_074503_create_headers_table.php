<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('headers', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('picturePath');
          $table->string('title');
          $table->string('oneliner');
          $table->enum('status', ['Active', 'Deactive'])->default('Active');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('headers');
    }
}
