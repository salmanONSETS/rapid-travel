@extends('app')
@section('content')
<?php
$teamHeader = \App\Image::where('status', 'Active')->where('for', 'teamHeader')->first();
?>
@if(!empty($teamHeader))
<div class="page-title-area black-overlay text-center" style="background-image: url({{asset('images/').'/'.$teamHeader->picturePath}});">
@else
<div class="page-title-area black-overlay text-center" style="background-image: url({{asset('img/page-title-bg.jpg')}});">
@endif
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="page-title-inner">
          <div class="page-title-inner-table-cell">
            <h1>Team Member</h1>
            <div class="page-title-menu">
              <ul>
                <li><a href="/">Home</a></li>
                <li><a href="javascript:void(0)">Team member</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
$teamMembers = \App\TeamMember::where('status', 'Active')->get();
?>
@if(!$teamMembers->isEmpty())
<div class="destinations-page-content-area padding-top">
  <div class="team-members-area text-center">
    <div class="container">
      <div class="row">
        @foreach($teamMembers as $teamMember)
        <div class="col-md-3 col-sm-6">
          <div class="single-team-member padding-bottom">
            <div class="team-member-image" style="background-image:url({{ asset('team/images/').'/'.$teamMember->picturePath }})">
              <div class="team-member-image-inner">
              </div>
            </div>
            <h4>{{$teamMember->name}}</h4>
            <p>{{$teamMember->designation}}</p>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
@endif
@endsection
