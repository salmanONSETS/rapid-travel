<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ToastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.toasts');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $toast = new \App\Toast;
        $toast->message = $request->get('message');
        $toast->status = 'Active';
        $toast->useage = $request->get('useage');
        $toast->save();
        return redirect('toasts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $toast = \App\Toast::find($id);
        return view('dashboard.toasts')->with('toast', $toast);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $oldToasts = \App\Toast::where('status', 'Active')->where('useage', $request->get('useage'))->get();
      foreach ($oldToasts as $key => $value) {
        $value->status = 'Deactive';
        $value->save();
      }
      $toast = \App\Toast::find($id);
      $toast->message = $request->get('message');
      $toast->status = 'Active';
      $toast->useage = $request->get('useage');
      $toast->save();
      return redirect('toasts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $toast = \App\Toast::find($id);
        $toast->delete();
        return redirect('toasts');
    }

    public function changestatus($id){
      $toast = \App\Toast::find($id);
      if($toast->status == 'Active'){
        $toast->status = 'Deactive';
      }
      else{
        $toast->status = 'Active';
      }
      $toast->save();
      return redirect('/toasts');
      //return view('dashboard.searchformimages');
    }
}
