@extends('app')
@section('content')
<?php
$tourHeader = \App\Image::where('status', 'Active')->where('for', 'tourHeader')->first();
?>
@if(!empty($tourHeader))
<div class="page-title-area black-overlay text-center" style="background-image: url({{asset('images/').'/'.$tourHeader->picturePath}});">
@else
<div class="page-title-area black-overlay text-center" style="background-image: url({{asset('img/page-title-bg.jpg')}});">
@endif
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="page-title-inner">
          <div class="page-title-inner-table-cell">
            <h1>Our Tours</h1>
            <div class="page-title-menu">
              <ul>
                <li><a href="/">Home</a></li>
                <li><a href="javascript:void(0)">Tours</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
$tours = \App\Tour::where('status', 'Active')->where('type', 'Custom')->get();
?>
@if(!$tours->isEmpty())
<!--Destination page content area start -->
<div class="destinations-page-content-area section-padding">
  <div class="sea-tours-columns padding-bottom">
    <div class="container">
      <div class="row">
        @foreach($tours as $tour)
        <div class="col-sm-4">
          <div class="single-sea-tours-item">
            <?php
            $media = \App\Image::where('status', 'Active')->where('forId', $tour->id)->first();
            ?>
            <div class="single-sea-tours-image black-overlay" style="background-image: url({{ asset('images/').'/'.$media->picturePath }});">
              @if(!empty($tour->price))
              <a href="javascript:void(0)" class="pink-btn">{{$tour->price}}</a>
              @endif
              <h2>{{$tour->place}}</h2>
            </div>
            <div class="single-sea-tours-text">
              <h5>
                <a href="/tours/{{$tour->id}}">
                  {{$tour->title}}
                </a>
              </h5>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
<!--Destination page content area end -->
@endif
@endsection
