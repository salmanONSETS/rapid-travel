@extends('app')
@section('content')
<?php
$contactHeader = \App\Image::where('status', 'Active')->where('for', 'contactHeader')->first();
?>
@if(!empty($contactHeader))
<div class="page-title-area black-overlay text-center" style="background-image: url({{asset('images/').'/'.$contactHeader->picturePath}});">
@else
<div class="page-title-area black-overlay text-center" style="background-image: url({{asset('img/page-title-bg.jpg')}});">
@endif
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-title-inner">
                                <div class="page-title-inner-table-cell">
                                    <h1>Contact us</h1>
                                    <div class="page-title-menu">
                                        <ul>
                                            <li><a href="/">Home</a></li>
                                            <li><a href="javascript:void(0)">Contact us</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact-page-conent-area section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-7">
                       <div class="contact-page-form">
                           <h3>Get in touch</h3>
                           <form role="form" action="/contactrequests" method="post" enctype="multipart/form-data">
                             {{ csrf_field() }}
                               <div class="row">
                                   <div class="col-md-12">
                                       <label for="contact-us-name">Name<span class="requiredIcon">*</span></label>
                                       <input id="contact-us-name" name="name" placeholder="Enter you name" type="text" required>
                                   </div>
                                 </div>
                                 <div class="row">
                                   <div class="col-md-6">
                                       <label for="contact-us-email">Email<span class="requiredIcon">*</span></label>
                                       <input id="contact-us-email" name="email" placeholder="Enter your email" type="email">
                                   </div>
                                   <div class="col-md-6">
                                       <label for="contact-us-email">Phone<span class="requiredIcon">*</span></label>
                                       <input id="contact-us-email" name="phone" placeholder="Enter your phone (03001111111)" type="tel" pattern="[0-9]{11,20}" required>
                                   </div>
                               </div>
                               <label for="contact-us-subject">Subject<span class="requiredIcon">*</span></label>
                               <input id="contact-us-subject" name="subject" placeholder="Enter Subject" type="text" required>
                               <label for="contact-us-message">Message<span class="requiredIcon">*</span></label>
                               <textarea name="message" id="contact-us-message" cols="30" rows="10" placeholder="Enter your message"></textarea>
                               <button type="submit" class="pink-btn">Submit</button>
                           </form>
                       </div>
                    </div>
                    <div class="col-md-4 col-sm-5">
                        <div class="contact-us-right-area">
                            <h3>Contact Us</h3>
                            <?php
                            $addresses = \App\Address::where('status', 'Active')->get();
                            $contacts = \App\Contact::where('status', 'Active')->get();
                            $emails = \App\Email::where('status', 'Active')->get();
                            ?>
                            @if(!$addresses->isEmpty())
                            <a class="contact-info-box" href="javascript:void(0)"><i class="zmdi zmdi-home"></i>
                              @foreach($addresses as $address)
                              @if(!empty($address->title))
                              <b>{{$address->title}}:</b><br>
                              @endif
                              {{$address->address}}<br>
                              @endforeach
                            </a>
                            @endif
                            @if(!$contacts->isEmpty())
                            <a class="contact-info-box" href="javascript:void(0)"><i class="zmdi zmdi-phone"></i>
                              @foreach($contacts as $contact)
                              @if(!empty($contact->title))
                              <b>{{$contact->title}}:</b><br>
                              @endif
                              {{$contact->contact}}<br>
                              @endforeach
                            </a>
                            @endif
                            @if(!$emails->isEmpty())
                            <a class="contact-info-box" href="javascript:void(0)"><i class="zmdi zmdi-email"></i>
                              @foreach($emails as $email)
                              @if(!empty($email->title))
                              <b>{{$email->title}}:</b><br>
                              @endif
                              {{$email->email}}<br>
                              @endforeach
                            </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--contact-page-conent-area end -->
        <!--contact-map-area start -->
        <!--contact-map-area end -->
        <!--contact-page-socials-area start -->
        <?php
        $social = \App\Social::where('status', 'Active')->first();
        ?>
        @if(!empty($social))
        @if(!empty($social->map))
        <div class="contact-map-area padding-bottom">
          <iframe src="{{$social->map}}" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        @endif
        <div class="contact-page-socials-area text-center padding-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="social-links-icon">
                          @if($social->facebook)
                          <a href="{{$social->facebook}}"><i class="fa fa-facebook"></i></a>
                          @endif
                          @if($social->twitter)
                          <a href="{{$social->twitter}}"><i class="fa fa-twitter"></i></a>
                          @endif
                          @if($social->instagram)
                          <a href="{{$social->instagram}}"><i class="fa fa-instagram"></i></a>
                          @endif
                          @if($social->google)
                          <a href="{{$social->google}}"><i class="fa fa-google-plus"></i></a>
                          @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
@endsection
