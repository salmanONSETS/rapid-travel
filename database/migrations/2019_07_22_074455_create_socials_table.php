<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('socials', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('facebook')->nullable();
          $table->string('google')->nullable();
          $table->string('instagram')->nullable();
          $table->string('twitter')->nullable();
          $table->longtext('map')->nullable();
          $table->enum('status', ['Active', 'Deactive'])->default('Active');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('socials');
    }
}
