<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;
class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.images');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = new \App\Image;
        $image->for = $request->get('for');
        $image->status = 'Active';
        if($request->hasFile('image')){
          $destinationImages='images';
          $allowedImagefileExtension=['jpg','png','jpeg'];
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $image->picturePath = $filename;
          }
          else{
            $message = 'Only .png, .jpg, .jpeg accepted';
            return redirect('headerImages')->with('message', $message);
          }
        }
        $image->save();
        $old = \App\Image::where('for', $image->for)->where('status', 'Active')->where('id', '!=', $image->id)->get();
        foreach ($old as $key => $value) {
          $value->status = 'Deactive';
          $value->save();
        }
        return redirect('/headerImages');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $image = \App\Image::find($id);
        return view('dashboard.images')->with('image', $image);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = \App\Image::find($id);
        if($request->hasFile('image')){
          $destinationImages='images';
          $allowedImagefileExtension=['jpg','png','jpeg'];
          if(!empty($image->picturePath)){
            $path = 'images/'.$image->picturePath;
            $file=Input::file("image");
            $extension = $file->getClientOriginalExtension();
            $checkImage=in_array($extension,$allowedImagefileExtension);
            $filename = time() . '-' .$file->getClientOriginalName();
            if($checkImage){
              $file->move($destinationImages,$filename);
              $image->picturePath = $filename;
            }
            File::delete($path);
          }
          else{
            $file=Input::file("image");
            $extension = $file->getClientOriginalExtension();
            $checkImage=in_array($extension,$allowedImagefileExtension);
            $filename = time() . '-' .$file->getClientOriginalName();
            if($checkImage){
              $file->move($destinationImages,$filename);
              $image->picturePath = $filename;
            }
            else{
              $message = 'Only .png, .jpg, .jpeg accepted';
              return redirect('headerImages')->with('message', $message);
            }
          }
        }
        $image->save();
        $old = \App\Image::where('for', $image->for)->where('status', 'Active')->where('id', '!=', $image->id)->get();
        foreach ($old as $key => $value) {
          $value->status = 'Deactive';
          $value->save();
        }
        return redirect('/headerImages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $image = \App\Image::find($id);
      $path = 'images/'.$image->picturePath;
      File::delete($path);
      $gallery->delete();
      return redirect('/headerImages');
    }

    public function changestatus($id){
      $image = \App\Image::find($id);
      if($image->status == 'Active'){
        $image->status = 'Deactive';
      }
      else{
        $image->status = 'Active';
      }
      $image->save();
      return redirect('/headerImages');
      //return view('dashboard.searchformimages');
    }
}
