<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('images', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->enum('for', ['galleryHeader', 'tourHeader', 'contactHeader', 'teamHeader', 'aboutHeader', 'packageHeader', 'tour', 'package', 'loginHeader', 'registerHeader']);
          $table->integer('forId')->unsigned()->nullable();
          $table->string('picturePath');
          $table->enum('status', ['Active', 'Deactive'])->default('Active');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
