<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.cities');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $city = new \App\City;
        $city->name = $request->get('name');
        $city->status = 'Active';
        $city->save();
        return redirect('cities');
        //return view('dashboard.cities');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = \App\City::find($id);
        return view('dashboard.cities')->with('city', $city);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $city = \App\City::find($id);
        $city->name = $request->get('name');
        $city->save();
        return redirect('cities');
        //return view('dashboard.cities');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city = \App\City::find($id);
        $city->delete();
        return redirect('cities');
        //return view('dashboard.cities');
    }

    public function changestatus($id){
      $city = \App\City::find($id);
      if($city->status == 'Active'){
        $city->status = 'Deactive';
      }
      else{
        $city->status = 'Active';
      }
      $city->save();
      return redirect('cities');
    }
}
