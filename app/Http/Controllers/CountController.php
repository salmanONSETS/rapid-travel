<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;
class CountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.counts');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $count = new \App\Count;
      $count->title = $request->get('title');
      $count->count = $request->get('count');
      if($request->hasFile('image')){
        $destinationImages='count/images';
        $allowedImagefileExtension=['jpg','png','jpeg'];
        $file=Input::file("image");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          $file->move($destinationImages,$filename);
          $count->picturePath = $filename;
        }
        else{
          $message = 'Only .png, .jpg, .jpeg accepted';
          return redirect('counts')->with('message', $message);
        }
      }
      $count->status = 'Active';
      $count->save();
      return redirect('counts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $count = \App\Count::find($id);
        return view('dashboard.counts')->with('count', $count);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $count = \App\Count::find($id);
      $count->title = $request->get('title');
      $count->count = $request->get('count');
      if($request->hasFile('image')){
        $destinationImages='count/images';
        $allowedImagefileExtension=['jpg','png','jpeg'];
        $file=Input::file("image");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          if(!empty($count->picturePath)){
            $path = 'count/images/'.$count->picturePath;
            File::delete($path);
          }
          $file->move($destinationImages,$filename);
          $count->picturePath = $filename;
        }
        else{
          $message = 'Only .png, .jpg, .jpeg accepted';
          return redirect('counts')->with('message', $message);
        }
      }
      $count->status = 'Active';
      $count->save();
      return redirect('counts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $count = \App\Count::find($id);
      if(!empty($count->picturePath)){
        $path = 'count/images/'.$count->picturePath;
        File::delete($path);
      }
      $count->delete();
      return redirect('counts');
    }

    public function changestatus($id){
      $count = \App\Count::find($id);
      if($count->status == 'Active'){
        $count->status = 'Deactive';
      }
      else{
        $count->status = 'Active';
      }
      $count->save();
      return redirect('/counts');
      //return view('dashboard.searchformimages');
    }
}
