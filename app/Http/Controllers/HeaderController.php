<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;

class HeaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.headers');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $header = new \App\Header;
      $header->title = $request->get('title');
      $header->oneliner = $request->get('oneliner');
      $header->status = 'Active';

      $destinationImages='headers/images';
      $allowedImagefileExtension=['jpg','png','jpeg'];
      if($request->hasFile('image')){
        $file=Input::file("image");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          $file->move($destinationImages,$filename);
          $header->picturePath = $filename;
        }
        else{
          $message = 'Only .png, .jpg, .jpeg accepted';
          return redirect('header')->with('message', $message);
        }
      }

      $header->save();
      return redirect('header');
      //return view('dashboard.headers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $header = \App\Header::find($id);
        return view('dashboard.headers')->with('header', $header);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $header = \App\Header::find($id);
        $header->title = $request->get('title');
        $header->oneliner = $request->get('oneliner');
        $header->status = 'Active';

        $destinationImages='headers/images';
        $allowedImagefileExtension=['jpg','png','jpeg'];
        if($request->hasFile('image')){
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            if(!empty($header->picturePath)){
              $path = 'headers/images/'.$header->picturePath;
              $file->move($destinationImages,$filename);
              $header->picturePath = $filename;
              File::delete($path);
            }
            else{
              $file->move($destinationImages,$filename);
              $header->picturePath = $filename;
            }
          }
          else{
            $message = 'Only .png, .jpg, .jpeg accepted';
            return redirect('header')->with('message', $message);
          }
        }

        $header->save();
        return redirect('header');
        //return view('dashboard.headers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $header = \App\Header::find($id);
        $path = 'headers/images/'.$header->picturePath;
        File::delete($path);
        $header->delete();
        return redirect('header');
        //return view('dashboard.headers');
    }

    public function changestatus($id){
      $header = \App\Header::find($id);
      if($header->status == 'Active'){
        $header->status = 'Deactive';
      }
      else{
        $header->status = 'Active';
      }
      $header->save();
      return redirect('header');
    }
}
