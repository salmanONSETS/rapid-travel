<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;
class AboutusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.aboutus');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $aboutus = new \App\Aboutus;
        $aboutus->title = $request->get('title');
        $aboutus->detail = $request->get('detail');
        $destinationImages='aboutpage/images';
        $allowedImagefileExtension=['jpg','png','jpeg'];
        if($request->hasFile('largeImage')){
          $file=Input::file("largeImage");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $aboutus->largeImage = $filename;
          }
          else{
            $message = 'Only .png, .jpg, .jpeg accepted';
            return redirect('aboutus')->with('message', $message);
          }
        }
        if($request->hasFile('smallImage')){
          $file=Input::file("smallImage");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $aboutus->smallImage = $filename;
          }
          else{
            $message = 'Only .png, .jpg, .jpeg accepted';
            return redirect('aboutus')->with('message', $message);
          }
        }
        $aboutus->status = 'Active';
        $aboutus->save();
        return redirect('aboutus');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aboutus = \App\Aboutus::find($id);
        return view('dashboard.aboutus')->with('aboutus', $aboutus);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $aboutus = \App\Aboutus::find($id);
      $aboutus->title = $request->get('title');
      $aboutus->detail = $request->get('detail');
      $destinationImages='aboutpage/images';
      $allowedImagefileExtension=['jpg','png','jpeg'];
      if($request->hasFile('largeImage')){
        $file=Input::file("largeImage");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          if(!empty($aboutus->largeImage)){
            $path = 'aboutpage/images/'.$aboutus->largeImage;
            File::delete($path);
          }
          $file->move($destinationImages,$filename);
          $aboutus->largeImage = $filename;
        }
        else{
          $message = 'Only .png, .jpg, .jpeg accepted';
          return redirect('aboutus')->with('message', $message);
        }
      }
      if($request->hasFile('smallImage')){
        $file=Input::file("smallImage");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          if(!empty($aboutus->smallImage)){
            $path = 'aboutpage/images/'.$aboutus->smallImage;
            File::delete($path);
          }
          $file->move($destinationImages,$filename);
          $aboutus->smallImage = $filename;
        }
        else{
          $message = 'Only .png, .jpg, .jpeg accepted';
          return redirect('aboutus')->with('message', $message);
        }
      }
      $aboutus->status = 'Active';
      $aboutus->save();
      return redirect('aboutus');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $aboutus = \App\Aboutus::find($id);
      if(!empty($aboutus->largeImage)){
        $path = 'aboutpage/images/'.$aboutus->largeImage;
        File::delete($path);
      }
      $aboutus->delete();
      return redirect('aboutus');
    }

    public function changestatus($id){
      $aboutus = \App\Aboutus::find($id);
      if($aboutus->status == 'Active'){
        $aboutus->status = 'Deactive';
      }
      else{
        $aboutus->status = 'Active';
      }
      $aboutus->save();
      return redirect('aboutus');
    }

    public function deleteLargeImage($id){
      $aboutus = \App\Aboutus::find($id);
      if(!empty($aboutus->largeImage)){
        $path = 'aboutpage/images/'.$aboutus->largeImage;
        File::delete($path);
        $aboutus->largeImage = null;
        $aboutus->save();
      }
      return redirect('aboutus');
    }
}
