<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SocialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.socials');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $socials = \App\Social::all();
        if($socials->isEmpty()){
          $social = new \App\Social;
          $social->facebook = $request->get('facebook');
          $social->instagram = $request->get('instagram');
          $social->google = $request->get('google');
          $social->twitter = $request->get('twitter');
          $social->map = $request->get('map');
          $social->status = 'Active';
          $social->save();
        }
        else{
            foreach ($socials as $key => $value) {
              $value->status = 'Deactive';
              $value->save();
            }
            $social = new \App\Social;
            $social->facebook = $request->get('facebook');
            $social->instagram = $request->get('instagram');
            $social->google = $request->get('google');
            $social->twitter = $request->get('twitter');
            $social->map = $request->get('map');
            $social->status = 'Active';
            $social->save();

        }
        return redirect('socials');
        //return view('dashboard.socials');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $social = \App\Social::find($id);
        return view('dashboard.socials')->with('social', $social);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $social = \App\Social::find($id);
      $socials = \App\Social::all();
      if($socials->isEmpty()){
        $social->facebook = $request->get('facebook');
        $social->instagram = $request->get('instagram');
        $social->google = $request->get('google');
        $social->map = $request->get('map');
        $social->twitter = $request->get('twitter');
        $social->status = 'Active';
        $social->save();
      }
      else{
          foreach ($socials as $key => $value) {
            $value->status = 'Deactive';
            $value->save();
          }
          $social->facebook = $request->get('facebook');
          $social->instagram = $request->get('instagram');
          $social->google = $request->get('google');
          $social->map = $request->get('map');
          $social->twitter = $request->get('twitter');
          $social->status = 'Active';
          $social->save();
      }
      return redirect('socials');
      //return view('dashboard.socials');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $social = \App\Social::find($id);
        $social->delete();
        return redirect('socials');
        //return view('dashboard.socials');
    }

    public function changestatus($id){
      $social = \App\Social::find($id);
      if($social->status == 'Active'){
        $social->status = 'Deactive';
      }
      else{
        $socials = \App\Social::where('status', 'Active')->where('id', '!=', $id)->get();
        foreach ($socials as $key => $value) {
          $value->status = 'Deactive';
          $value->save();
        }
        $social->status = 'Active';
      }
      $social->save();
      return redirect('socials');
    }
}
