@extends('dashboard.dashboardlayout')
@section('content')
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($image))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Header Image</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/headerImages" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>For</label>
                    <select class="form-control" name="for" required>
                      <option value="contactHeader">Contact Header</option>
                      <option value="aboutHeader">About Header</option>
                      <option value="teamHeader">Team Header</option>
                      <option value="tourHeader">Tours Header</option>
                      <option value="registerHeader">Register Header</option>
                      <option value="loginHeader">Login Header</option>
                      <option value="packageHeader">Packages Header</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png" required>
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Header Image</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/headerImages/<?php echo $image->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>For</label>
                    <select class="form-control" name="for" required>
                      @if($image->for === 'contactHeader')
                      <option value="contactHeader" selected>Contact Header</option>
                      @else
                      <option value="contactHeader">Contact Header</option>
                      @endif
                      @if($image->for === 'aboutHeader')
                      <option value="aboutHeader" selected>About Header</option>
                      @else
                      <option value="aboutHeader">About Header</option>
                      @endif
                      @if($image->for === 'teamHeader')
                      <option value="teamHeader" selected>Team Header</option>
                      @else
                      <option value="teamHeader">Team Header</option>
                      @endif
                      @if($image->for === 'tourHeader')
                      <option value="tourHeader" selected>Tours Header</option>
                      @else
                      <option value="tourHeader">Tours Header</option>
                      @endif
                      @if($image->for === 'packageHeader')
                      <option value="packageHeader" selected>Packages Header</option>
                      @else
                      <option value="packageHeader">Packages Header</option>
                      @endif
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                    <small>If you want to change the old image you can upload the new one it will replace the old one. Otherwise, leave it empty.</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png">
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Header Images</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Image</th>
                    <th>For</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $images = \App\Image::where('forId', NULL)->get();
                  ?>
                  @foreach($images as $image)
                  <tr>
                    <td>{{$image->id}}</td>
                    <td><img src="{{asset('images/').'/'.$image->picturePath}}" alt="Header Image" width="100" height="100" /></td>
                    <td>{{$image->for}}</td>
                    <td>{{$image->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/headerImages_changestatus/<?php echo $image->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/headerImages/<?php echo $image->id;?>/edit">Edit</a>
                      <form action="{{ route('headerImages.destroy', $image->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
