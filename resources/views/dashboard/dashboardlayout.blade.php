<!doctype html>
<html lang="en">
  <head>
    <title>Rapid Travel Dashboard</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="description" content="Rapid Travel - Tour and Travel Agency Website">
    <meta name="keywords" content="Rapid Travel, Travel, Tour, Hotels, Cars, Flights">
    <meta name="author" content="ONSETS">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="icon" href="{{ asset('./img/logo.png') }}" type="image" sizes="16x16">
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- Material Kit CSS -->
    <link href="{{ asset('css/dashboard/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/now-ui-dashboard.css') }}" rel="stylesheet" />

  </head>
  <body>
    <div class="wrapper">
    <div class="sidebar" data-color="orange">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
  -->
      <div class="logo">
          <a href="http://www.creative-tim.com" class="simple-text logo-mini">
              RT
          </a>
          <a href="http://www.creative-tim.com" class="simple-text logo-normal">
            Rapid Travel
          </a>
      </div>

      <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
          <?php
          $name = Route::currentRouteName();
          ?>
          @if(strpos($name, 'queries') || strpos($name, 'queries') === 0)
          <li class="active">
              <a href="/queries">
                    <i class="now-ui-icons design_app"></i>
                  <p>Queries</p>
              </a>
          </li>
          @else
          <li>
              <a href="/queries">
                    <i class="now-ui-icons design_app"></i>
                  <p>Queries</p>
              </a>
          </li>
          @endif
          @if(strpos($name, 'contactrequests') || strpos($name, 'contactrequests') === 0)
          <li class="active">
              <a href="/contactrequests">
                    <i class="now-ui-icons education_atom"></i>
                  <p>Contact Requests</p>
              </a>
          </li>
          @else
          <li>
              <a href="/contactrequests">
                    <i class="now-ui-icons education_atom"></i>
                  <p>Contact Requests</p>
              </a>
          </li>
          @endif
          <!--@if(strpos($name, 'cities') || strpos($name, 'cities') === 0)
          <li class="active">
              <a href="/cities">
                    <i class="now-ui-icons business_globe"></i>
                  <p>Cities</p>
              </a>
          </li>
          @else
          <li>
              <a href="/cities">
                    <i class="now-ui-icons business_globe"></i>
                  <p>Cities</p>
              </a>
          </li>
          @endif-->
          @if(strpos($name, 'addresses') || strpos($name, 'addresses') === 0)
          <li class="active">
              <a href="/addresses">
                    <i class="now-ui-icons location_map-big"></i>
                  <p>Addresses</p>
              </a>
          </li>
          @else
          <li>
              <a href="/addresses">
                    <i class="now-ui-icons location_map-big"></i>
                  <p>Addresses</p>
              </a>
          </li>
          @endif
          @if(strpos($name, 'contacts') || strpos($name, 'contacts') === 0)
          <li class="active">
              <a href="/contacts">
                    <i class="now-ui-icons tech_mobile"></i>
                  <p>Contacts</p>
              </a>
          </li>
          @else
          <li>
              <a href="/contacts">
                    <i class="now-ui-icons tech_mobile"></i>
                  <p>Contacts</p>
              </a>
          </li>
          @endif
          @if(strpos($name, 'emails') || strpos($name, 'emails') === 0)
          <li class="active">
              <a href="/emails">
                    <i class="now-ui-icons ui-1_email-85"></i>
                  <p>Emails</p>
              </a>
          </li>
          @else
          <li>
              <a href="/emails">
                    <i class="now-ui-icons ui-1_email-85"></i>
                  <p>Emails</p>
              </a>
          </li>
          @endif
          @if(strpos($name, 'socials') || strpos($name, 'socials') === 0)
          <li class="active">
              <a href="/socials">
                    <i class="now-ui-icons ui-2_like"></i>
                  <p>Socials</p>
              </a>
          </li>
          @else
          <li>
              <a href="/socials">
                    <i class="now-ui-icons ui-2_like"></i>
                  <p>Socials</p>
              </a>
          </li>
          @endif
          <!--@if(strpos($name, 'gallery') || strpos($name, 'gallery') === 0)
          <li class="active">
              <a href="/gallery">
                    <i class="now-ui-icons media-1_album"></i>
                  <p>Gallery</p>
              </a>
          </li>
          @else
          <li>
              <a href="/gallery">
                    <i class="now-ui-icons media-1_album"></i>
                  <p>Gallery</p>
              </a>
          </li>
          @endif-->
          @if(strpos($name, 'header') || strpos($name, 'header') === 0)
          <li class="active">
              <a href="/header">
                    <i class="now-ui-icons arrows-1_minimal-up"></i>
                  <p>Headers</p>
              </a>
          </li>
          @else
          <li>
              <a href="/header">
                    <i class="now-ui-icons arrows-1_minimal-up"></i>
                  <p>Headers</p>
              </a>
          </li>
          @endif
          @if(strpos($name, 'searchformimages') || strpos($name, 'searchformimages') === 0)
          <li class="active">
              <a href="/searchformimages">
                    <i class="now-ui-icons files_single-copy-04"></i>
                  <p>Search Form Images</p>
              </a>
          </li>
          @else
          <li>
              <a href="/searchformimages">
                    <i class="now-ui-icons files_single-copy-04"></i>
                  <p>Search Form Images</p>
              </a>
          </li>
          @endif
          @if(strpos($name, 'teamMembers') || strpos($name, 'teamMembers') === 0)
          <li class="active">
              <a href="/teamMembers">
                    <i class="now-ui-icons design_vector"></i>
                  <p>Team</p>
              </a>
          </li>
          @else
          <li>
              <a href="/teamMembers">
                    <i class="now-ui-icons design_vector"></i>
                  <p>Team</p>
              </a>
          </li>
          @endif
          @if(strpos($name, 'tours') || strpos($name, 'tours') === 0)
          <li class="active">
              <a href="/tours">
                    <i class="now-ui-icons design_vector"></i>
                  <p>Tours</p>
              </a>
          </li>
          @else
          <li>
              <a href="/tours">
                    <i class="now-ui-icons design_vector"></i>
                  <p>Tours</p>
              </a>
          </li>
          @endif
          @if(strpos($name, 'packages') || strpos($name, 'packages') === 0)
          <li class="active">
              <a href="/packages">
                    <i class="now-ui-icons design_vector"></i>
                  <p>Umrah Packages</p>
              </a>
          </li>
          @else
          <li>
              <a href="/packages">
                    <i class="now-ui-icons design_vector"></i>
                  <p>Umrah Packages</p>
              </a>
          </li>
          @endif
          @if(strpos($name, 'headerImages') || strpos($name, 'headerImages') === 0)
          <li class="active">
              <a href="/headerImages">
                    <i class="now-ui-icons design_vector"></i>
                  <p>Header Images</p>
              </a>
          </li>
          @else
          <li>
              <a href="/headerImages">
                    <i class="now-ui-icons design_vector"></i>
                  <p>Header Images</p>
              </a>
          </li>
          @endif
          @if(strpos($name, 'bookings') || strpos($name, 'bookings') === 0)
          <li class="active">
              <a href="/bookings">
                    <i class="now-ui-icons design_vector"></i>
                  <p>Bookings</p>
              </a>
          </li>
          @else
          <li>
              <a href="/bookings">
                    <i class="now-ui-icons design_vector"></i>
                  <p>Bookings</p>
              </a>
          </li>
          @endif
          @if(strpos($name, 'aboutus') || strpos($name, 'aboutus') === 0)
          <li class="active">
              <a href="/aboutus">
                    <i class="now-ui-icons design_vector"></i>
                  <p>About Us</p>
              </a>
          </li>
          @else
          <li>
              <a href="/aboutus">
                    <i class="now-ui-icons design_vector"></i>
                  <p>About Us</p>
              </a>
          </li>
          @endif
          @if(strpos($name, 'features') || strpos($name, 'features') === 0)
          <li class="active">
              <a href="/features">
                    <i class="now-ui-icons design_vector"></i>
                  <p>Features</p>
              </a>
          </li>
          @else
          <li>
              <a href="/features">
                    <i class="now-ui-icons design_vector"></i>
                  <p>Features</p>
              </a>
          </li>
          @endif
          @if(strpos($name, 'counts') || strpos($name, 'counts') === 0)
          <li class="active">
              <a href="/counts">
                    <i class="now-ui-icons design_vector"></i>
                  <p>Counts</p>
              </a>
          </li>
          @else
          <li>
              <a href="/counts">
                    <i class="now-ui-icons design_vector"></i>
                  <p>Counts</p>
              </a>
          </li>
          @endif
          @if(strpos($name, 'toasts') || strpos($name, 'toasts') === 0)
          <li class="active">
              <a href="/toasts">
                    <i class="now-ui-icons design_vector"></i>
                  <p>Toasts</p>
              </a>
          </li>
          @else
          <li>
              <a href="/toasts">
                    <i class="now-ui-icons design_vector"></i>
                  <p>Toasts</p>
              </a>
          </li>
          @endif
        </ul>
      </div>
    </div>
    <div class="main-panel ps" id="main-panel">
    <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
      				</button>
      			</div>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="now-ui-icons users_single-02"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="/logout">Logout</a>
                </div>
              </li>
            </ul>
    	    </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="panel-header panel-header-sm">
      </div>
      @yield('content')
      <footer class="footer">
        <div class=" container-fluid ">
          <div class="copyright" id="copyright">
            © <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script>, Designed by <a href="https://www.invisionapp.com" target="_blank">Invision</a>. Coded by <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>.
            Developed by <a href="http://onsets.co/" target="_blank">ONSETS</a>
          </div>
        </div>
    </footer>
    <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div>
    <div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div>
  </div>
  </div>
</body>
  <!--   Core JS Files   -->
  <script src="{{ asset('./js/core/jquery.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('./js/core/popper.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('./js/core/bootstrap.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('./js/plugins/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('./js/plugins/bootstrap-notify.js') }}" type="text/javascript"></script>
  <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
  <script>
  CKEDITOR.replace( 'details' );
  CKEDITOR.replace( 'detail' );
  </script>
  <script type="text/javascript">
          $(document).ready(function(){
              $.notify({
                  message: @yield('message')
                },{
                    type: @yield('barcolor'),
                    timer: 4000
                });
          });
      </script>
      @if(session('message'))
      <script type="text/javascript">
      $(document).ready(function(){
        $.notify({
          message: "{{session('message')}}"
        },
        {
          type: 'danger',
          allow_dismiss: true,
          timer: 4000
        });
      });
      </script>
      @endif
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('./js/now-ui-dashboard.js') }}" type="text/javascript"></script>
</html>
