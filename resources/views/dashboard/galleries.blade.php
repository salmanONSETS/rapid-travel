@extends('dashboard.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($gallery))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Gallery</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/gallery" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Place</label>
                    <input type="text" class="form-control" placeholder="Place" name="place" required>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png" required>
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Gallery</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/gallery/<?php echo $gallery->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" required value="{{$gallery->title}}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Place</label>
                    <input type="text" class="form-control" placeholder="Place" name="place" required value="{{$gallery->place}}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                    <small>If you want to change the old image you can upload the new one it will replace the old one. Otherwise, leave it empty.</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png">
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Gallery</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Place</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $galleries = \App\Gallery::all();
                  ?>
                  @foreach($galleries as $gallery)
                  <tr>
                    <td>{{$gallery->id}}</td>
                    <td><img src="{{asset('galleries/images/').'/'.$gallery->picturePath}}" alt="Project Image" width="100" height="100" /></td>
                    <td>{{$gallery->title}}</td>
                    <td>{{$gallery->place}}</td>
                    <td>{{$gallery->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/gallery_changestatus/<?php echo $gallery->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/gallery/<?php echo $gallery->id;?>/edit">Edit</a>
                      <form action="{{ route('gallery.destroy', $gallery->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
