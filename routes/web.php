<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/viewTours', function(){
  return view('viewTours');
});

Route::get('/viewPackages', function(){
  return view('viewPackages');
});

Route::get('/viewTeamMembers', function(){
  return view('viewTeam');
});

/*Route::get('/tours', function () {
    return view('viewAllTours');
});*/

Route::resource('/contactrequests', 'ContactRequestController');

Route::get('/login-panel', 'UserController@loginpanel');
Route::get('/register-panel', 'UserController@registerpanel');

Auth::routes();

Route::get('/logout', 'UserController@logout');

//Counts
Route::resource('/counts', 'CountController');
Route::get('/counts_changestatus/{id}', 'CountController@changestatus');

//Toasts
Route::resource('/toasts', 'ToastController');
Route::get('/toasts_changestatus/{id}', 'ToastController@changestatus');

//Features
Route::resource('/features', 'FeatureController');
Route::get('/features_changestatus/{id}', 'FeatureController@changestatus');

//About us
Route::resource('/aboutus', 'AboutusController');
Route::get('/aboutus_changestatus/{id}', 'AboutusController@changestatus');
Route::get('/deleteLargeImage/{id}', 'AboutusController@deleteLargeImage');

//Contact Request
Route::get('/contactrequests_changestatus/{id}', 'ContactRequestController@changestatus');
Route::get('/contactrequests_delete/{id}', 'ContactRequestController@destroy');

//Queries
Route::resource('/queries', 'QueryController');
Route::get('/queries_changestatus/{id}', 'QueryController@changestatus');

//Gallery
Route::resource('/gallery', 'GalleryController');
Route::get('/gallery_changestatus/{id}', 'GalleryController@changestatus');

//Search Form Image
Route::resource('/searchformimages', 'SearchFormImageController');
Route::get('/searchformimages_changestatus/{id}', 'SearchFormImageController@changestatus');

//Headers
Route::resource('/header', 'HeaderController');
Route::get('/header_changestatus/{id}', 'HeaderController@changestatus');

//Addresses
Route::resource('/addresses', 'AddressController');
Route::get('/addresses_changestatus/{id}', 'AddressController@changestatus');

//Contact
Route::resource('/contacts', 'ContactController');
Route::get('/contacts_changestatus/{id}', 'ContactController@changestatus');

//Emails
Route::resource('/emails', 'EmailController');
Route::get('/emails_changestatus/{id}', 'EmailController@changestatus');

//Socials
Route::resource('/socials', 'SocialController');
Route::get('/socials_changestatus/{id}', 'SocialController@changestatus');

//Cities
Route::resource('/cities', 'CityController');
Route::get('/cities_changestatus/{id}', 'CityController@changestatus');

//Team
Route::resource('/teamMembers', 'TeamMemberController');
Route::get('/teamMembers_changestatus/{id}', 'TeamMemberController@changestatus');

//Tour
Route::resource('/tours', 'TourController');
Route::get('/deleteTourImage/{id}/{tourid}', 'TourController@deleteTourImage');
Route::get('/tours_changestatus/{id}', 'TourController@changestatus');


//Package
Route::resource('/packages', 'PackageController');
Route::get('/deletePackageImage/{id}/{packageid}', 'PackageController@deletePackageImage');
Route::get('/packages_changestatus/{id}', 'PackageController@changestatus');

//Images
Route::resource('/headerImages', 'ImageController');
Route::get('/headerImages_changestatus/{id}', 'ImageController@changestatus');

//Booking
Route::resource('/bookings', 'BookingController');
Route::get('/bookings_changestatus/{id}', 'BookingController@changestatus');
