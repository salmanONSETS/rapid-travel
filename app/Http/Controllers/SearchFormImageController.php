<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;
use Illuminate\Routing\Redirector;
class SearchFormImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.searchformimages');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $searchformimage = new \App\SearchFormImage;
      $searchformimage->status = 'Active';

      $destinationImages='formimages/images';
      $allowedImagefileExtension=['jpg','png','jpeg'];
      if($request->hasFile('image')){
        $file=Input::file("image");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          $file->move($destinationImages,$filename);
          $searchformimage->picturePath = $filename;
        }
        else{
          $message = 'Only .png, .jpg, .jpeg accepted';
          return redirect('searchformimages')->with('message', $message);
        }
      }

      $searchformimage->save();
      return redirect('searchformimages');
      //return view('dashboard.searchformimages');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $searchformimage = \App\SearchFormImage::find($id);
      $path = 'formimages/images/'.$searchformimage->picturePath;
      File::delete($path);
      $searchformimage->delete();
      return redirect('searchformimages');
      //return view('dashboard.searchformimages');
    }

    public function changestatus($id){
      $searchformimage = \App\SearchFormImage::find($id);
      if($searchformimage->status == 'Active'){
        $searchformimage->status = 'Deactive';
      }
      else{
        $searchformimage->status = 'Active';
      }
      $searchformimage->save();
      return redirect('searchformimages');
      //return view('dashboard.searchformimages');
    }
}
