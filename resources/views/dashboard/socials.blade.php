@extends('dashboard.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <?php
        $socials = \App\Social::all();
        ?>
        @if(!isset($social) && $socials->isEmpty())
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Socials</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/socials" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Facebook</label>
                    <input type="url" class="form-control" placeholder="Facebook" name="facebook">
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Google Plus</label>
                    <input type="url" class="form-control" placeholder="Google Plus" name="google">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Instagram</label>
                    <input type="url" class="form-control" placeholder="Instagram" name="instagram">
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Twitter</label>
                    <input type="url" class="form-control" placeholder="Twitter" name="twitter">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <label for="exampleInputEmail1">Google Maps Link</label>
                  <input type="url" class="form-control" placeholder="Google Maps Link" name="map">
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Socials</h5>
          </div>
          <?php
          $social = \App\Social::first();
          ?>
          <div class="card-body">
            <form role="form" action="/socials/<?php echo $social->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Facebook</label>
                    <input type="url" class="form-control" placeholder="Facebook" name="facebook" value="{{$social->facebook}}">
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Google Plus</label>
                    <input type="url" class="form-control" placeholder="Google Plus" name="google" value="{{$social->google}}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Instagram</label>
                    <input type="url" class="form-control" placeholder="Instagram" name="instagram" value="{{$social->instagram}}">
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Twitter</label>
                    <input type="url" class="form-control" placeholder="Twitter" name="twitter" value="{{$social->twitter}}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <label for="exampleInputEmail1">Google Maps Link</label>
                  <input type="url" class="form-control" placeholder="Google Maps Link" name="map" value="{{$social->map}}">
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Socials</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Facebook</th>
                    <th>Google</th>
                    <th>Instagram</th>
                    <th>Twitter</th>
                    <th>Google Maps</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $socials = \App\Social::all();
                  ?>
                  @foreach($socials as $social)
                  <tr>
                    <td>{{$social->id}}</td>
                    <td><a href="{{$social->facebook}}" target="_blank">Facebook</a></td>
                    <td><a href="{{$social->google}}" target="_blank">Google Plus</a></td>
                    <td><a href="{{$social->instagram}}" target="_blank">Instagram</a></td>
                    <td><a href="{{$social->twitter}}" target="_blank">Twitter</a></td>
                    <td><a href="{{$social->map}}" target="_blank">Google Maps</a></td>
                    <td>{{$social->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="socials_changestatus/<?php echo $social->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/socials/<?php echo $social->id;?>/edit">Edit</a>
                      <form action="{{ route('socials.destroy', $social->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
