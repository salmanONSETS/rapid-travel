@extends('dashboard.dashboardlayout')
@section('content')
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($header))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Header</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/header" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>One Liner</label>
                    <input type="text" class="form-control" placeholder="One Liner" name="oneliner" required>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png" required>
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Header</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/header/<?php echo $header->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" required value="{{$header->title}}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>One Liner</label>
                    <input type="text" class="form-control" placeholder="One Liner" name="oneliner" required value="{{$header->oneliner}}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png">
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Headers</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Image</th>
                    <th>Title</th>
                    <th>One Liner</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $headers = \App\Header::all();
                  ?>
                  @foreach($headers as $header)
                  <tr>
                    <td>{{$header->id}}</td>
                    <td><img src="{{asset('headers/images/').'/'.$header->picturePath}}" alt="Project Image" width="100" height="100" /></td>
                    <td>{{$header->title}}</td>
                    <td>{{$header->oneliner}}</td>
                    <td>{{$header->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/header_changestatus/<?php echo $header->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/header/<?php echo $header->id;?>/edit">Edit</a>
                      <form action="{{ route('header.destroy', $header->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
