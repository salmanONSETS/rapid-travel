<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.contactrequests');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contactUs = new \App\ContactRequest;
        $contactUs->name = $request->get('name');
        $contactUs->email = $request->get('email');
        $contactUs->phone = $request->get('phone');
        $contactUs->subject = $request->get('subject');
        $contactUs->message = $request->get('message');
        $contactUs->save();
        $toast = \App\Toast::where('status', 'Active')->where('useage', 'ContactRequest')->first();
        if(!empty($toast)){
          $message = $toast->message;
        }
        else{
          $message = 'Received';
        }
        return redirect('contact')->with('message', $message);
        //return view('contact');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contactRequest = \App\ContactRequest::find($id);
        return view('dashboard.contactrequests')->with('contactRequest', $contactRequest);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contactRequest = \App\ContactRequest::find($id);
        $contactRequest->delete();
        return redirect('contactrequests');
        //return view('dashboard.contactrequests');
    }

    public function changestatus($id){
      $contactRequest = \App\ContactRequest::find($id);
      if($contactRequest->status == 'Active'){
        $contactRequest->status = 'Deactive';
      }
      else{
        $contactRequest->status = 'Active';
      }
      $contactRequest->save();
      return redirect('contactrequests');
    }
}
