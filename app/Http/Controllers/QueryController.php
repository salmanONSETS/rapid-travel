<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QueryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.queries');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = new \App\Query;
        $query->name = $request->get('name');
        $query->phone = $request->get('phone');
        $query->email = $request->get('email');
        if($request->get('noOfAdults') != ''){
            $query->noOfAdults = $request->get('noOfAdults');
        }
        if($request->get('noOfInfants') != ''){
            $query->noOfInfants = $request->get('noOfInfants');
        }
        if($request->get('noOfKids') != ''){
            $query->noOfKids = $request->get('noOfKids');
        }
        /*if($request->get('minimumPrice')){
            $query->minimumPrice = $request->get('minimumPrice');
        }
        if($request->get('maximumPrice')){
            $query->maximumPrice = $request->get('maximumPrice');
        }*/
        if($request->get('from')){
            $query->from = $request->get('from');
        }
        if($request->get('to')){
            $query->to = $request->get('to');
        }
        if($request->get('city') != ""){
          $query->city = $request->get('city');
        }
        if($request->get('fromCity') != ""){
          $query->fromCity = $request->get('fromCity');
        }
        if($request->get('toCity') != ""){
          $query->toCity = $request->get('toCity');
        }
        if($request->get('category') == "null"){
          $query->category = null;
        }
        else{
          $query->category = $request->get('category');
        }
        $query->type = $request->get('type');
        $query->status = "Active";
        $query->save();
        $toast = \App\Toast::where('status', 'Active')->where('useage', 'Query')->first();
        if(!empty($toast)){
          $message = $toast->message;
        }
        else{
          $message = 'Received';
        }
        return redirect('/')->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query = \App\Query::find($id);
        return view('dashboard.queries')->with('query', $query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = \App\Query::find($id);
        $query->delete();
        return redirect('queries');
        //return view('dashboard.queries');
    }

    public function changestatus($id){
      $query = \App\Query::find($id);
      if($query->status == 'Active'){
        $query->status = 'Deactive';
      }
      else{
        $query->status = 'Active';
      }
      $query->save();
      return redirect('queries');
    }
}
