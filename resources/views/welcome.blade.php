@extends('app')
@section('content')
<div class="slider-area slider-area-hp1 owl-carousel text-center">
  <?php
  $headers = \App\Header::where('status', 'Active')->get();
  ?>
  @foreach($headers as $header)
  <div class="single-slide-item" data-dot="<img src='{{ asset('headers/images/').'/'.$header->picturePath }}' alt=''>"  style="background-image: url({{ asset('headers/images/').'/'.$header->picturePath }}); background-repeat: no-repeat; background-size: cover;">
    <div class="single-slide-item-table-cell">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1 text-center">
            <h1>{{$header->title}}</h1>
            <p>{{$header->oneliner}}</p>
            <a href="/viewTours" class="pink-btn">View All Tours</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endforeach
</div>
<div class="slider-bottom-form-carousel-area-hp1 padding-top">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <div class="slider-bottom-form-hp1">
          <div class="slider-bottom-menu">
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Flights</a></li>
              <li role="presentation"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Hotels</a></li>
              <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Customized Tours</a></li>
              <!--<li role="presentation" hidden><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Car rent</a></li> -->
            </ul>
          </div>
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in" id="home">
              <div class="s-bottom-form">
                <form role="form" action="/queries" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <input type="text" name="type" value="Hotels" hidden>
                  <div class="row">
                    <div class="col-sm-12">
                      <p class="name-input-arrow">
                        <input type="text" id="hotel-name-input" name="name" placeholder="Your Name" required>
                      </p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <input type="tel" pattern="[0-9]{11,20}" id="hotel-name-input" name="phone" placeholder="Phone (e.g: 03001111111)" required>
                    </div>
                    <div class="col-sm-6">
                      <p class="name-input-arrow">
                        <input type="email" id="hotel-name-input" name="email" placeholder="Email" required>
                      </p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <input type="text" name="city" placeholder="City" />
                    </div>
                    <div class="col-sm-6">
                      <select name="category">
                        <option value="null">Choose Category</option>
                        <option>5 Star</option>
                        <option>4 Star</option>
                        <option>3 Star</option>
                        <option>2 Star</option>
                      </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <input type="number" min="0" id="hotel-name-input" name="noOfAdults" placeholder="Adults">
                    </div>
                    <div class="col-sm-6">
                      <input type="number" min="0" id="hotel-name-input" name="noOfKids" placeholder="Kids">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <input type="number" min="0" id="hotel-name-input" name="minimumPrice" placeholder="Minimum Price" hidden>
                    </div>
                    <div class="col-sm-6">
                      <input type="number" min="0" id="hotel-name-input" name="maximumPrice" placeholder="Maximum Price" hidden>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <label>Check In</label>
                      <p class="date-input-arrow"><input type="date" name="from" placeholder="Check In"></p>
                    </div>
                    <div class="col-sm-6">
                      <label>Check Out</label>
                      <p class="date-input-arrow"><input type="date" name="to" placeholder="Check Out"></p>
                    </div>
                  </div>
                  <button type="submit" class="pink-btn">Book Hotels</button>
                </form>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="profile">
              <div class="s-bottom-form">
                <form role="form" action="/queries" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <input type="text" name="type" value="Tours" hidden>
                  <div class="row">
                    <div class="col-sm-6">
                      <p class="name-input-arrow">
                        <input type="text" id="hotel-name-input" name="name" placeholder="Your Name" required>
                      </p>
                    </div>
                    <div class="col-sm-6">
                      <input type="tel" pattern="[0-9]{11,20}" id="hotel-name-input" name="phone" placeholder="Phone (e.g: 03001111111)" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <p class="name-input-arrow">
                        <input type="email" id="hotel-name-input" name="email" placeholder="Email" required>
                      </p>
                    </div>
                    <?php $cities = \App\City::where('status', 'Active')->get(); ?>
                    <div class="col-sm-6">
                      <input type="text" name="city" placeholder="City" />
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <input type="number" id="hotel-name-input" min="0" name="noOfAdults" placeholder="Adults">
                    </div>
                    <div class="col-sm-6">
                      <input type="number" id="hotel-name-input" min="0" name="noOfKids" placeholder="Kids">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <input type="number" id="hotel-name-input" min="0" name="minimumPrice" placeholder="Minimum Price" hidden>
                    </div>
                    <div class="col-sm-6">
                      <input type="number" id="hotel-name-input" min="0" name="maximumPrice" placeholder="Maximum Price" hidden>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <label>From</label>
                      <p class="date-input-arrow"><input type="date" name="from"></p>
                    </div>
                    <div class="col-sm-6">
                      <label>To</label>
                      <p class="date-input-arrow"><input type="date" name="to"></p>
                    </div>
                  </div>
                  <button type="submit" class="pink-btn">Book Customized Tours</button>
                </form>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="messages">
              <div class="s-bottom-form">
                <form role="form" action="/queries" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <input type="text" name="type" value="Car Rents" hidden>
                  <div class="row">
                    <div class="col-sm-6">
                      <p class="name-input-arrow">
                        <input type="text" id="hotel-name-input" name="name" placeholder="Your Name" required>
                      </p>
                    </div>
                    <div class="col-sm-6">
                      <input type="tel" pattern="[0-9]{11,20}" id="hotel-name-input" name="phone" placeholder="Phone (e.g: 03001111111)" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <p class="name-input-arrow">
                        <input type="email" id="hotel-name-input" name="email" placeholder="Email" required>
                      </p>
                    </div>
                    <?php $cities = \App\City::where('status', 'Active')->get(); ?>
                    <div class="col-sm-6">
                      <select name="cityId" id="city-select">
                        <option value="null">Choose City</option>
                        @foreach($cities as $city)
                        <option value="{{$city->id}}">{{ $city->name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <input type="number" min="0" id="hotel-name-input" name="noOfAdults" placeholder="Adults">
                    </div>
                    <div class="col-sm-6">
                      <input type="number" min="0" id="hotel-name-input" name="noOfKids" placeholder="Kids">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <input type="number" min="0" id="hotel-name-input" name="minimumPrice" placeholder="Minimum Price">
                    </div>
                    <div class="col-sm-6">
                      <input type="number" min="0" id="hotel-name-input" name="maximumPrice" placeholder="Maximum Price">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <p class="date-input-arrow"><input type="date" name="from"></p>
                    </div>
                    <div class="col-sm-6">
                      <p class="date-input-arrow"><input type="date" name="to"></p>
                    </div>
                  </div>
                  <button type="submit" class="pink-btn">Find Cars</button>
                </form>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade in active" id="settings">
              <div class="s-bottom-form">
                <form role="form" action="/queries" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <input type="text" name="type" value="Flights" hidden>
                  <div class="row">
                    <div class="col-sm-6">
                      <p class="name-input-arrow">
                        <input type="text" id="hotel-name-input" name="name" placeholder="Your Name" required>
                      </p>
                    </div>
                    <div class="col-sm-6">
                      <input type="tel" pattern="[0-9]{11,20}" id="hotel-name-input" name="phone" placeholder="Phone (e.g: 03001111111)" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <p class="name-input-arrow">
                        <input type="email" id="hotel-name-input" name="email" placeholder="Email" required>
                      </p>
                    </div>
                    <div class="col-sm-6">
                      <input type="number" min="0" id="hotel-name-input" name="noOfAdults" placeholder="Adults">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <input type="number" min="0" id="hotel-name-input" name="noOfKids" placeholder="Children">
                    </div>
                    <div class="col-sm-6">
                      <input type="number" min="0" id="hotel-name-input" name="noOfInfants" placeholder="Infants">
                    </div>
                  </div>
                  <div class="row">
                    <?php $cities = \App\City::where('status', 'Active')->get(); ?>
                    <div class="col-sm-6">
                      <input type="text" name="fromCity" placeholder="From City" />
                    </div>
                    <div class="col-sm-6">
                      <input type="text" name="toCity" placeholder="To City" />
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <input type="number" min="0" id="hotel-name-input" name="minimumPrice" placeholder="Minimum Price" hidden>
                    </div>
                    <div class="col-sm-6">
                      <input type="number" min="0" id="hotel-name-input" name="maximumPrice" placeholder="Maximum Price" hidden>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <label>Departure</label>
                      <p class="date-input-arrow"><input type="date" name="from"></p>
                    </div>
                    <div class="col-sm-6">
                      <label>Return</label>
                      <p class="date-input-arrow"><input type="date" name="to"></p>
                    </div>
                  </div>
                  <button type="submit" class="pink-btn">Book Flights</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="slider-bottom-gallery-hp1 owl-carousel">
          <?php
          $formImages = \App\SearchFormImage::where('status', 'Active')->get();
          ?>
          @foreach($formImages as $formImage)
          <div class="slider-bottom-single-gallery" data-dot="<img src='{{ asset('formimages/images/').'/'.$formImage->picturePath }}' alt=''>">
            <img src="{{ asset('formimages/images/').'/'.$formImage->picturePath }}" alt="">
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
<div class="section-sea-tours-area">
  <div class="section-title text-center section-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <h1>Our Best Tours</h1>
          <p>Explore a different way to travel</p>
        </div>
      </div>
    </div>
  </div>
  <div class="sea-tours-columns padding-bottom">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="sea-tours-carousel-hp1-columns owl-carousel">
            <?php
            $tours = \App\Tour::where('status', 'Active')->get();
            ?>
            @foreach($tours as $tour)
            <?php
            $media = \App\Image::where('status', 'Active')->where('forId', $tour->id)->first();
            ?>
            <div class="single-sea-tours-item">
              <div class="single-sea-tours-image black-overlay" style="background-image: url({{ asset('images/').'/'.$media->picturePath }});">
                <h2>{{$tour->place}}</h2>
              </div>
              <div class="single-sea-tours-text">
                <h5><a href="/tours/{{$tour->id}}">
                  {{$tour->title}}
                </a></h5>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="padding-bottom footer-top-area" style="background-image: url({{ asset('img/section-sea-tours-paris-bg.jpg') }}); background-size:cover; background-position:center">
  <div class="section-title text-center section-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <h1>Our Achievements</h1>
          <p>Explore a different way to travel</p>
        </div>
      </div>
    </div>
  </div>
  <div class="counter-up-area text-center">
    <div class="container">
      <div class="row">
        <?php
        $counts = \App\Count::where('status', 'Active')->get();
        ?>
        @foreach($counts as $value)
        <div class="col-sm-3">
          <div class="single-counter-up-item">
            <div class="single-counter-up-item-image">
              <img src="{{asset('count/images/').'/'.$value->picturePath}}" alt="Count Image" width="48" height="48" />
            </div>
            <div class="single-counter-up-item-text">
              <h1><span class="counter">{{$value->count}}</span></h1>
              <p>{{$value->title}}</p>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
@endsection
