@extends('app')
@section('content')
<?php
$loginHeader = \App\Image::where('status', 'Active')->where('for', 'loginHeader')->first();
?>
@if(!empty($loginHeader))
<div class="page-title-area black-overlay text-center" style="background-image: url({{asset('images/').'/'.$loginHeader->picturePath}});">
@else
<div class="page-title-area black-overlay text-center" style="background-image: url({{asset('img/page-title-bg.jpg')}});">
@endif
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="page-title-inner">
          <div class="page-title-inner-table-cell">
            <h1>Login</h1>
            <div class="page-title-menu">
              <ul>
                <li><a href="/">Home</a></li>
                <li><a href="#">Login</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="login-page-content-area section-padding">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <div class="login-page-logo text-center">
          <img src="{{ asset('./img/footer-logo.png') }}" alt="">
          <p>Please login to your account</p>
        </div>
        <div class="login-form-div">
          <form role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}
            <input type="email" name="email" placeholder="Email">
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
            <input type="password" name="password" placeholder="Password">
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
            <button type="submit">Login Now</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
