<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;

class TeamMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.teamMembers');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $teamMember = new \App\TeamMember;
        $teamMember->name = $request->get('name');
        $teamMember->designation = $request->get('designation');
        $teamMember->status = 'Active';

        $destinationImages='team/images';
        $allowedImagefileExtension=['jpg','png','jpeg'];
        if($request->hasFile('image')){
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $teamMember->picturePath = $filename;
          }
        }

        $teamMember->save();
        return redirect('/teamMembers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $teamMember = \App\TeamMember::find($id);
      return view('dashboard.teamMembers')->with('teamMember', $teamMember);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $teamMember = \App\TeamMember::find($id);
      $teamMember->name = $request->get('name');
      $teamMember->designation = $request->get('designation');

      $destinationImages='team/images';
      $allowedImagefileExtension=['jpg','png','jpeg'];
      if($request->hasFile('image')){
        if(!empty($teamMember->picturePath)){
          $path = 'team/images/'.$teamMember->picturePath;
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $teamMember->picturePath = $filename;
          }
          File::delete($path);
        }
        else{
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $teamMember->picturePath = $filename;
          }
        }
      }

      $teamMember->save();
      return redirect('/teamMembers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $teamMember = \App\TeamMember::find($id);
      $path = 'team/images/'.$teamMember->picturePath;
      File::delete($path);
      $teamMember->delete();
      return redirect('/teamMembers');
    }

    public function changestatus($id){
      $teamMember = \App\TeamMember::find($id);
      if($teamMember->status == 'Active'){
        $teamMember->status = 'Deactive';
      }
      else{
        $teamMember->status = 'Active';
      }
      $teamMember->save();
      return redirect('/teamMembers');
    }
}
