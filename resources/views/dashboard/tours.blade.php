@extends('dashboard.dashboardlayout')
@section('content')
<div class="content">
  <div class="row">
    <div class="col-md-12">
      @if(!isset($tour))
      <div class="card">
        <div class="card-header">
          <h5 class="title">Add Tour</h5>
        </div>
        <div class="card-body">
          <form role="form" action="/tours" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Title</label>
                  <input type="text" class="form-control" placeholder="Title" name="title" required>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Price</label>
                  <input type="number" min="0" class="form-control" placeholder="Price" name="price">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Place</label>
                  <input type="text" class="form-control" placeholder="Place" name="place" required>
                  <input type="text" value="Custom" name="type" hidden>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                  <button class="btn btn-primary">
                    Choose Files<input type="file" class="form-control" name="images[]" accept=".jpg,.jpeg,.png" multiple required>
                  </button>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>Details</label>
                  <textarea rows="10" class="form-control" placeholder="Details" name="details"></textarea>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
          </form>
        </div>
      </div>
      @else
      <div class="card">
        <div class="card-header">
          <h5 class="title">Edit Tour</h5>
        </div>
        <div class="card-body">
          <?php
          $images = \App\Image::where('forId', $tour->id)->where('status', 'Active')->get();
          ?>
          <div class="row">
            @foreach($images as $image)
            <div class="col-md-4 text-center">
              <img src="{{asset('images/').'/'.$image->picturePath}}" alt="Project Image" />
              <a type="link" class="btn btn-danger btn-sm" href="/deleteTourImage/<?php echo $image->id;?>/<?php echo $tour->id; ?>">Delete</a>
            </div>
            @endforeach
          </div>
          <form role="form" action="/tours/<?php echo $tour->id;?>" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{method_field('PUT')}}
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Title</label>
                  <input type="text" class="form-control" placeholder="Title" name="title" required value="{{$tour->title}}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Price</label>
                  <input type="number" class="form-control" min="0" placeholder="Price" name="price" value="{{$tour->price}}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Place</label>
                  <input type="text" class="form-control" placeholder="Place" name="place" required value="{{$tour->place}}">
                  <input type="text" value="Custom" name="type" hidden>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                  <button class="btn btn-primary">
                    Choose Files<input type="file" class="form-control" name="images[]" accept=".jpg,.jpeg,.png" multiple>
                  </button>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>Details</label>
                  <textarea rows="10" class="form-control" placeholder="Details" name="details">{{$tour->details}}</textarea>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
          </form>
        </div>
      </div>
      @endif
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Tours</h4>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table">
              <thead class=" text-primary">
                <tr>
                  <th>Id</th>
                  <th>Title</th>
                  <th>Price</th>
                  <th>Place</th>
                  <th>Status</th>
                  <th class="text-right">Control Section</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $tours = \App\Tour::where('type', 'Custom')->get();
                ?>
                @foreach($tours as $tour)
                <tr>
                  <td>{{$tour->id}}</td>
                  <td>{{$tour->title}}</td>
                  <td>{{$tour->price}}</td>
                  <td>{{$tour->place}}</td>
                  <td>{{$tour->status}}</td>
                  <td class="text-right">
                    <a type="link" class="btn btn-default btn-sm" href="/tours_changestatus/<?php echo $tour->id;?>">Change Status</a>
                    <a type="link" class="btn btn-warning btn-sm" href="/tours/<?php echo $tour->id;?>">View</a>
                    <a type="link" class="btn btn-info btn-sm" href="/tours/<?php echo $tour->id;?>/edit">Edit</a>
                    <form action="{{ route('tours.destroy', $tour->id) }}" method="post" style="display:inline">
                      {{ method_field('DELETE') }}
                      {{ csrf_field() }}
                      <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                    </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
