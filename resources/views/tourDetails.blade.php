@extends('app')
@section('content')
<!--tours-details-area-content start -->
@if(!empty($tour))
<div class="tours-details-area-content section-padding">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="tours-details-content-left">
          <div class="tours-gallery-heading">
            <h2>{{$tour->title}}</h2>
          </div>
          <?php
          $images = \App\Image::where('forId', $tour->id)->get();
          ?>
          <div class="tours-details-gallery owl-carousel padding-bottom">
            @foreach($images as $image)
            <div class="tours-details-gallery-single" data-dot="<img src='{{ asset('images/').'/'.$image->picturePath }}' alt='{{$tour->title}}'>">
              <img src="{{ asset('images/').'/'.$image->picturePath }}" alt="Tour Image">
            </div>
            @endforeach
          </div>
          <div class="tours-details-menu">
            <ul class="nav nav-tabs" role="tablist">
              <li class="active"><a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">Detail</a></li>
            </ul>
          </div>
          <div class="tab-content tours-details-tab-content padding-top">
            <div role="tabpanel" class="tab-pane fade in active" id="detail">
              <div class="tours-details-menu-bottom">
                <h2>About {{$tour->place}}</h2>
                <p>{!! $tour->details !!}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="tours-details-content-right">
          <div class="right-details-form padding-bottom">
            <h3><span>Price</span> {{$tour->price}}</h3>
            <h4>Book the tour</h4>
            <form role="form" action="/bookings" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <input type="text" placeholder="Name" name="name">
              <input type="text" name="tourId" value="{{$tour->id}}" hidden>
              <input type="email" placeholder="Email" name="email">
              <input type="tel" pattern="[0-9]{11,20}" placeholder="Phone" name="phone">
              <textarea name="message" id="details-message" cols="30" rows="10" placeholder="Message"></textarea>
              <button type="submit" class="pink-btn">Book now</button>
            </form>
          </div>
          <div class="why-book-us">
            <h4>Why Book With Us?</h4>
            <a href="javascript:void(0)" class="why-book-us-btn"><i class="zmdi zmdi-money"></i> No-hassle best price guarantee</a>
            <a href="javascript:void(0)" class="why-book-us-btn"><i class="zmdi zmdi-account"></i> Customer care available 24/7</a>
            <a href="javascript:void(0)" class="why-book-us-btn"><i class="zmdi zmdi-account"></i> Free Travel Insureance</a>
          </div>
          <div class="need-travel-help">
            <h4>Need Travel Help?</h4>
            <p>Do not hesitage to give us a call. We are an expert team and we are happy to talk to you.</p>
            <?php
            $contact = \App\Contact::where('status', 'Active')->where('type', 'Primary')->first();
            $email = \App\Email::where('status', 'Active')->where('type', 'Primary')->first();
            ?>
            @if(!empty($contact))
            <h3><i class="zmdi zmdi-phone"></i>
              {{$contact->contact}}
            </h3>
            @endif
            @if(!empty($email))
            <a href="mailto:{{$email->email}}"><i class="zmdi zmdi-email"></i>
              {{$email->email}}
            </a>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
@endsection
