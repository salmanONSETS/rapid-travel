<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.galleries');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gallery = new \App\Gallery;
        $gallery->title = $request->get('title');
        $gallery->place = $request->get('place');
        $gallery->status = 'Active';

        $destinationImages='galleries/images';
        $allowedImagefileExtension=['jpg','png','jpeg'];
        if($request->hasFile('image')){
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $gallery->picturePath = $filename;
          }
          else{
            $message = 'Only .png, .jpg, .jpeg accepted';
            return redirect('gallery')->with('message', $message);
          }
        }

        $gallery->save();
        return redirect('gallery');
        //return view('dashboard.galleries');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallery = \App\Gallery::find($id);
        return view('dashboard.galleries')->with('gallery', $gallery);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $gallery = \App\Gallery::find($id);
      $gallery->title = $request->get('title');
      $gallery->place = $request->get('place');

      $destinationImages='galleries/images';
      $allowedImagefileExtension=['jpg','png','jpeg'];
      if($request->hasFile('image')){
        if(!empty($gallery->picturePath)){
          $path = 'galleries/images/'.$gallery->picturePath;
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $gallery->picturePath = $filename;
          }
          File::delete($path);
        }
        else{
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $gallery->picturePath = $filename;
          }
          else{
            $message = 'Only .png, .jpg, .jpeg accepted';
            return redirect('gallery')->with('message', $message);
          }
        }
      }

      $gallery->save();
      return redirect('gallery');
      //return view('dashboard.galleries');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = \App\Gallery::find($id);
        $path = 'galleries/images/'.$gallery->picturePath;
        File::delete($path);
        $gallery->delete();
        return redirect('gallery');
        //return view('dashboard.galleries');
    }

    public function changestatus($id){
      $gallery = \App\Gallery::find($id);
      if($gallery->status == 'Active'){
        $gallery->status = 'Deactive';
      }
      else{
        $gallery->status = 'Active';
      }
      $gallery->save();
      return redirect('gallery');
    }
}
