@extends('dashboard.dashboardlayout')
@section('content')
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($count))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Count</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/counts" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Count</label>
                    <input type="number" class="form-control" placeholder="Count" name="count" required>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label><small>Size: 48*48 (Only .png, .jpg, .jpeg accepted)</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png" required>
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Count</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-3">
                <img src="{{asset('count/images/').'/'.$count->picturePath}}" alt="Feature Image" width="48" height="48" />
              </div>
            </div>
            <form role="form" action="/counts/<?php echo $count->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" value="{{$count->title}}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Count</label>
                    <input type="number" class="form-control" placeholder="Count" name="count" value="{{$count->count}}">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label><small>Size: 48*48 (Only .png, .jpg, .jpeg accepted)</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png">
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Counts</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Count</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $counts = \App\Count::all();
                  ?>
                  @foreach($counts as $value)
                  <tr>
                    <td>{{$value->id}}</td>
                    <td><img src="{{asset('count/images/').'/'.$value->picturePath}}" alt="Feature Image" width="48" height="48" /></td>
                    <td>{{$value->title}}</td>
                    <td>{{$value->count}}</td>
                    <td>{{$value->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/counts_changestatus/<?php echo $value->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/counts/<?php echo $value->id;?>/edit">Edit</a>
                      <form action="{{ route('counts.destroy', $value->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
