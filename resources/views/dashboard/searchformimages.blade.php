@extends('dashboard.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Search Form Image</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/searchformimages" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png" required>
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Search Form Images</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Image</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $searchformimages = \App\SearchFormImage::all();
                  ?>
                  @foreach($searchformimages as $searchformimage)
                  <tr>
                    <td>{{$searchformimage->id}}</td>
                    <td><img src="{{asset('formimages/images/').'/'.$searchformimage->picturePath}}" alt="Search Form Image" width="100" height="100" /></td>
                    <td>{{$searchformimage->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/searchformimages_changestatus/<?php echo $searchformimage->id;?>">Change Status</a>
                      <form action="{{ route('searchformimages.destroy', $searchformimage->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
