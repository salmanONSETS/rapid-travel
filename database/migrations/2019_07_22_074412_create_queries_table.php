<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQueriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('queries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->integer('noOfAdults')->nullable();
            $table->integer('noOfKids')->nullable();
            $table->integer('noOfInfants')->nullable();
            $table->integer('minimumPrice')->nullable();
            $table->integer('maximumPrice')->nullable();
            $table->date('from')->nullable();
            $table->date('to')->nullable();
            $table->string('city')->nullable();
            $table->string('fromCity')->nullable();
            $table->string('toCity')->nullable();
            $table->enum('type', ['Hotels', 'Car Rents', 'Flights', 'Tours']);
            $table->enum('category', ['5 Star', '4 Star', '3 Star', '2 Star'])->nullable();
            $table->enum('status', ['Active', 'Deactive', 'Contacted'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('queries');
    }
}
