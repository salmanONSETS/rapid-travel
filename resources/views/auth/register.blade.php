@extends('app')
@section('content')
<?php
$registerHeader = \App\Image::where('status', 'Active')->where('for', 'registerHeader')->first();
?>
@if(!empty($registerHeader))
<div class="page-title-area black-overlay text-center" style="background-image: url({{asset('images/').'/'.$registerHeader->picturePath}});">
@else
<div class="page-title-area black-overlay text-center" style="background-image: url({{asset('img/page-title-bg.jpg')}});">
@endif
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="page-title-inner">
          <div class="page-title-inner-table-cell">
            <h1>Register</h1>
            <div class="page-title-menu">
              <ul>
                <li><a href="index-2.html">Home</a></li>
                <li><a href="#">Register</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="login-page-content-area section-padding">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <div class="login-page-logo text-center">
          <img src="assets/img/footer-logo.png" alt="">
          <p>Please register your account</p>
        </div>
        <div class="login-form-div">
          <form role="form" method="POST" action="{{ url('/register') }}">
            {{ csrf_field() }}
            <input type="text" name="name" placeholder="Name" value="{{ old('name') }}" required autofocus>
            @if ($errors->has('name'))
            <span class="help-block">
              <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
            <input type="email" name="email" placeholder="Email" value="{{ old('email') }}">
            @if ($errors->has('email'))
            <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
            <input type="password" name="password" placeholder="Password">
            @if ($errors->has('password'))
            <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
            <input type="password" placeholder="Confirm Password" name="password_confirmation">
            <input type="checkbox" name="checkbox" id="login-checkbox" value="value" required>
            <label for="login-checkbox">I agree to the terms of use and privacy.</label>
            <button type="submit">Register Now</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
