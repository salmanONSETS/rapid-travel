@extends('dashboard.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($feature))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Feature</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/features" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Image</label><small>Size: 47*47 (Only .png, .jpg, .jpeg accepted)</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png" required>
                    </button>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Description</label>
                    <textarea rows="10" class="form-control" placeholder="Description" name="description" required></textarea>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Feature</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-3">
                <img src="{{asset('feature/images/').'/'.$feature->picturePath}}" alt="Feature Image" width="47" height="47" />
              </div>
            </div>
            <form role="form" action="/features/<?php echo $feature->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" value="{{$feature->title}}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Image</label><small>Size: 47*47 (Only .png, .jpg, .jpeg accepted)</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png">
                    </button>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Description</label>
                    <textarea rows="10" class="form-control" placeholder="Description" name="description">{{$feature->description}}</textarea>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Features</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $features = \App\Feature::all();
                  ?>
                  @foreach($features as $value)
                  <tr>
                    <td>{{$value->id}}</td>
                    <td><img src="{{asset('feature/images/').'/'.$value->picturePath}}" alt="Feature Image" width="47" height="47" /></td>
                    <td>{{$value->title}}</td>
                    <td>{{$value->description}}</td>
                    <td>{{$value->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/features_changestatus/<?php echo $value->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/features/<?php echo $value->id;?>/edit">Edit</a>
                      <form action="{{ route('features.destroy', $value->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
