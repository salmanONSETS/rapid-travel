@extends('dashboard.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($city))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add City</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/cities" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>City</label>
                    <input type="text" class="form-control" placeholder="City" name="name" required>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit City</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/cities/<?php echo $city->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>City</label>
                    <input type="text" class="form-control" placeholder="City" name="name" required value="{{$city->name}}">
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Cities</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $cities = \App\City::all();
                  ?>
                  @foreach($cities as $city)
                  <tr>
                    <td>{{$city->id}}</td>
                    <td>{{$city->name}}</td>
                    <td>{{$city->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/cities_changestatus/<?php echo $city->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/cities/<?php echo $city->id;?>/edit">Edit</a>
                      <form action="{{ route('cities.destroy', $city->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
