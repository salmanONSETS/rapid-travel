@extends('app')
@section('content')
<?php
$aboutHeader = \App\Image::where('status', 'Active')->where('for', 'aboutHeader')->first();
$aboutus = \App\Aboutus::where('status', 'Active')->first();
?>
@if(!empty($aboutHeader))
<div class="page-title-area black-overlay text-center" style="background-image: url({{asset('images/').'/'.$aboutHeader->picturePath}});">
@else
<div class="page-title-area black-overlay text-center" style="background-image: url({{asset('img/page-title-bg.jpg')}});">
@endif
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="page-title-inner">
          <div class="page-title-inner-table-cell">
            <h1>About us</h1>
            <div class="page-title-menu">
              <ul>
                <li><a href="index-2.html">Home</a></li>
                <li><a href="javascript:void(0)">About</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@if(!empty($aboutus))
<div class="aboutus-page-content-area section-padding">
  <div class="welcome-to-travel-area">
    <div class="container">
      <div class="row">
        <div class="col-md-7">
          <div class="welcome-to-travel-inner">
            <div class="welcome-to-travel-inner-left">
              <h2>{{$aboutus->title}}</h2>
              <p>{!! $aboutus->detail !!}</p>
            </div>
          </div>
        </div>
        <div class="col-md-5">
          <div class="welcome-to-travel-inner-right">
            @if(empty($aboutus->largeImage))
            <img src="{{ asset('img/about-us-top-img1.jpg') }}" alt="About us Image">
            @else
            <img src="{{ asset('aboutpage/images/').'/'.$aboutus->largeImage }}" alt="About us Image">
            @endif
          </div>
        </div>
      </div>
    </div>
  </div><br>
  <div class="footer-top-area">
    <div class="section-title text-center section-padding">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
            <h1>Our Achievements</h1>
            <p>Explore a different way to travel</p>
          </div>
        </div>
      </div>
    </div>
    <div class="counter-up-area text-center">
      <div class="container">
        <div class="row">
          <?php
          $counts = \App\Count::where('status', 'Active')->get();
          ?>
          @foreach($counts as $value)
          <div class="col-sm-3">
            <div class="single-counter-up-item">
              <div class="single-counter-up-item-image">
                <img src="{{asset('count/images/').'/'.$value->picturePath}}" alt="Count Image" width="48" height="48" />
              </div>
              <div class="single-counter-up-item-text">
                <h1><span class="counter">{{$value->count}}</span></h1>
                <p>{{$value->title}}</p>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
  <div class="section-title text-center why-best-title-about padding-bottom">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3"><br><br>
          <h1>Why We Are Best</h1>
          <p>Explore a different way to travel</p>
        </div>
      </div>
    </div>
  </div>
  <div class="best-for-clients-columns text-center hp2-area-bg about-us-best-for-c">
    <div class="container">
      <div class="row">
        <?php
        $features = \App\Feature::where('status', 'Active')->get();
        ?>
        @foreach($features as $value)
        <div class="col-sm-4">
          <div class="best-for-clients-single-item">
            <img src="{{ asset('feature/images/').'/'.$value->picturePath }}" alt="{{$value->picturePath}}">
            <h4>{{$value->title}}</h4>
            <p>{{$value->description}}</p>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
@endif
@endsection
