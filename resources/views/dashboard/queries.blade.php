@extends('dashboard.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(isset($query))
        <div class="card">
          <div class="card-header">
            <h5 class="title">View Query ({{$query->type}})</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <h6>Personal Information</h6>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <label>Name:</label>
                @if(!empty($query->name))
                <span>{{$query->name}}</span>
                @endif
              </div>
              <div class="col-md-4">
                <label>Phone:</label>
                @if(!empty($query->phone))
                <span>{{$query->phone}}</span>
                @endif
              </div>
              <div class="col-md-4">
                <label>Email:</label>
                @if(!empty($query->email))
                <span>{{$query->email}}</span>
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <h6>People</h6>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <label>Adults:</label>
                @if(!empty($query->noOfAdults))
                <span>{{$query->noOfAdults}}</span>
                @endif
              </div>
              <div class="col-md-4">
                <label>Kids:</label>
                @if(!empty($query->noOfKids))
                <span>{{$query->noOfKids}}</span>
                @endif
              </div>
              @if(!empty($query->noOfInfants))
              <div class="col-md-4">
                <label>Infants:</label>
                <span>{{$query->noOfInfants}}</span>
              </div>
              @endif
            </div>
            <div class="row">
              <div class="col-md-12">
                <h6>Dates</h6>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                @if($query->type == 'Hotels' || $query->type == 'Tours')
                <label>Check In:</label>
                @else
                <label>Departure:</label>
                @endif
                @if(!empty($query->from))
                <span>{{$query->from}}</span>
                @endif
              </div>
              <div class="col-md-4">
                @if($query->type == 'Hotels' || $query->type == 'Tours')
                <label>Check Out:</label>
                @else
                <label>Return:</label>
                @endif
                @if(!empty($query->to))
                <span>{{$query->to}}</span>
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <h6>Places</h6>
              </div>
            </div>
            <div class="row">
              @if(!empty($query->fromCity))
              <div class="col-md-4">
                <label>From:</label>
                <span>{{$query->fromCity}}</span>
              </div>
              @endif
              @if(!empty($query->toCity))
              <div class="col-md-4">
                <label>To:</label>
                <span>{{$query->toCity}}</span>
              </div>
              @endif
              @if(!empty($query->city))
              <div class="col-md-4">
                <label>City:</label>
                <span>{{$query->city}}</span>
              </div>
              @endif
            </div>
            <div class="row" hidden>
              <div class="col-md-12">
                <h6>Prices</h6>
              </div>
            </div>
            <div class="row" hidden>
              <div class="col-md-4">
                <label>Minimum Price:</label>
                @if(!empty($query->minimumPrice))
                <span>{{$query->minimumPrice}}</span>
                @endif
              </div>
              <div class="col-md-4">
                <label>Maximum Price:</label>
                @if(!empty($query->maximumPrice))
                <span>{{$query->maximumPrice}}</span>
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <h6>Additional</h6>
              </div>
            </div>
            <div class="row">
              @if(!empty($query->category))
              <div class="col-md-4">
                <label>Category:</label>
                <span>{{$query->category}}</span>
              </div>
              @endif
            </div>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Queries</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $queries = \App\Query::all();
                  ?>
                  @foreach($queries as $query)
                  <tr>
                    <td>{{$query->id}}</td>
                    <td>{{$query->name}}</td>
                    <td>{{$query->email}}</td>
                    <td>{{$query->phone}}</td>
                    <td>{{$query->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-primary btn-sm" href="/queries/<?php echo $query->id; ?>">View</a>
                      <a type="link" class="btn btn-default btn-sm" href="/queries_changestatus/<?php echo $query->id;?>">Change Status</a>
                      <form action="{{ route('queries.destroy', $query->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
