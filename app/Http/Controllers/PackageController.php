<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;
use Illuminate\Routing\Redirector;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.packages');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $tour = new \App\Tour;
      $tour->price = $request->get('price');
      $tour->title = $request->get('title');
      $tour->place = $request->get('place');
      $tour->details = $request->get('details');
      $tour->type = $request->get('type');
      $tour->status = 'Active';
      $tour->save();

      if($request->hasFile('images')){
        $destinationImages='images';
        $allowedImagefileExtension=['jpg','png','jpeg'];
        $files=Input::file("images");
        foreach ($files as $key => $file) {
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $media = new \App\Image;
            $url = $destinationImages.'/'.$filename;
            $file->move($destinationImages,$filename);
            $media->picturePath = $filename;
            $media->for = "package";
            $media->forId = $tour->id;
            $media->status = "Active";
            $media->save();
          }
          else{
            $message = 'Only .png, .jpg, .jpeg accepted';
            return redirect('/packages')->with('message', $message);
          }
        }
      }
      return redirect('/packages');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $tour = \App\Tour::find($id);
      return view('packageDetails')->with('tour', $tour);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $tour = \App\Tour::find($id);
      return view('dashboard.packages')->with('tour', $tour);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $tour = \App\Tour::find($id);
      $tour->price = $request->get('price');
      $tour->title = $request->get('title');
      $tour->place = $request->get('place');
      $tour->details = $request->get('details');
      $tour->type = $request->get('type');
      $tour->status = 'Active';
      $tour->save();

      if($request->hasFile('images')){
        $destinationImages='images';
        $allowedImagefileExtension=['jpg','png','jpeg'];
        $files=Input::file("images");
        foreach ($files as $key => $file) {
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $media = new \App\Image;
            $url = $destinationImages.'/'.$filename;
            $file->move($destinationImages,$filename);
            $media->picturePath = $filename;
            $media->for = "package";
            $media->forId = $tour->id;
            $media->status = "Active";
            $media->save();
          }
          else{
            $message = 'Only .png, .jpg, .jpeg accepted';
            return redirect('/packages')->with('message', $message);
          }
        }
      }
      return redirect('/packages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $tour = \App\Tour::find($id);
      $images = \App\Image::where('forId', $id)->get();
      foreach ($images as $key => $value) {
        $path = 'images/'.$value->picturePath;
        File::delete($path);
        $value->delete();
      }
      $tour->delete();
      return redirect('/packages');
    }

    public function changestatus($id){
      $tour = \App\Tour::find($id);
      if($tour->status == 'Active'){
        $tour->status = 'Deactive';
      }
      else{
        $tour->status = 'Active';
      }
      $tour->save();
      return redirect('/packages');
      //return view('dashboard.searchformimages');
    }

    public function deleteTourImage($id, $tourid){
      $image = \App\Image::find($id);
      $path = 'images/'.$image->picturePath;
      File::delete($path);
      $image->delete();
      $tour = \App\Tour::find($tourid);
      return view('dashboard.packages')->with('tour', $tour);
      //return redirect('/tours');
    }
}
